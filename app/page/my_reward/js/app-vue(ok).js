var app = new Vue({
    el: '#experience',
    data: {
        tabs: ["体验金", "奖励"],
        isActive: 0,
        listData: [],
        // topShow: true,
        c:{},
        mescroll: null,
        blank:false,
        page:1,
        size:15
    },
    created: function(){
        var _this = this;
        // if(window.mobileUtil.isAndroid){
        //     _this.topShow = false;
        // }
        _this.c=new Cloud();

    },
    mounted:function(){
        var _this = this;
        //下拉刷新
        _this.mescroll = new MeScroll('mescroll',{
            down: {
                callback: function(){
                    _this.mescroll.endSuccess();
                }
            },
            up: {
                isBounce: false,
                page:{
                    num:1,
                    size: 15
                },
                callback: _this.init,
                empty:{
                    warpId: 'aaa',
                    tip: '暂无数据哦'
                }
            }
        })
    },
    methods: {
        init: function(page){
            var _this= this;

            _this.getListData(page.num,page.size,function(currentPageData){
                if(page.num === 1){
                    _this.listData=[]
                }
                _this.listData = _this.listData.concat(currentPageData);
                _this.mescroll.endSuccess(currentPageData.length);
                _this.page += 1;

            },function(){
                if(_this.page === 1){
                    _this.blank = true;
                }
                _this.blank = true;
                _this.mescroll.endErr()
            })
        },
        getListData: function(pageNo,limit,successCallback,errorCallback){
            var _this = this;
            var params = {
                "interface": "account/wallet/myRewards",
                "type": (_this.isActive+4).toString(),
                "limit": limit.toString(),
                "pageNo": pageNo.toString()
            };
            params = JSON.stringify(params);
            _this.c.gettransmitdata(params,function(data){
                if(data.code == '0' && data.result.walletRecords.length!=0){
                    var imitateData = [],
                    len = data.result.walletRecords.length;
                    for(var i = 0; i< len; i++ ){
                        if(i==data.result.walletRecords.length) break;
                        data.result.walletRecords[i].amount = tofix2float(data.result.walletRecords[i].amount);
                        data.result.walletRecords[i].time = timetrans(data.result.walletRecords[i].modifyTime);
                        imitateData.push(data.result.walletRecords[i])
                    }
                    successCallback&&successCallback(imitateData)
                }else{
                    if(pageNo === 1){
                        _this.blank = true;
                    }
                    errorCallback&&errorCallback()
                }
            });
        },
        tabChange: function(index){
            var _this = this;
            _this.isActive = index;
            this.page = 1;
            //这里用直接调用此方法在tab切换的时候会有报错。
            //_this.init();
            //我们改用他自己的自动下拉刷新的方法就行
            _this.mescroll.triggerDownScroll();
            _this.mescroll.resetUpScroll();
        },
        goBack: function(){
            var _this = this;
            _this.c.toFinish();
        },
        showMsg: function (txt) {
            $.popupTxt("body", "<span class='show'>" + txt + "</span>", 2000);
        }
    }
});

function timetrans(str){
    var date = new Date(str);
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
    var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes());
    var s = ":"+(date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
    return Y+M+D+h+m;
}

function tofix2float(value) {
    if (value === 0) {
        return '0.00';
    }
    var newstring = parseInt(value).toLocaleString('en-US'),
        intnum = newstring.indexOf(".") === -1 ? newstring : newstring.slice(0, newstring.indexOf(".")),
        floatnum = value.toString().indexOf(".") === -1 ? ".00" : value.toString().substr(value.toString().indexOf("."), 3);
    return intnum + floatnum;
}
