      
 var app = new Vue({
    el:'#Plans',
    data:{
        timer:0,
        timer1:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        res:0,
        bindBtn:0,
        rule:false,
        // step:'1',
        c:{}
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            _this.$nextTick(function(){
                $.getJSON(_this.c.Root+"newYear/gethistory.json?data="+_this.token, function(e){
                    // $.loaded();
                    console.log(e)
                    if(e.status == 200){
                        _this.res = e.data;
                        $.loaded();
                        $("#Plans").show();
                    }
                });
            })
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
            switch(key){
                case "goProductList":
                        _this.stat('pro');
                    break;     
                }
           return false;
        },
        //了解详情
        goRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('see');
                _this.rule=true;
            })
        },
        // 关闭规则
        closeRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.rule = false;
            })
        },
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('dream',d);
        }
    }
 });