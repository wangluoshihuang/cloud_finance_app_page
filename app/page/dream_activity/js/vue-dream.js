      
 var app = new Vue({
    el:'#Dream',
    data:{
        timer:0,
        timer1:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        res:{},
        bindBtn:0,
        val:[],
        tit:['买房','买车','结婚','教育','旅行','养老'],
        title:'',
        step:'1',
        status:0,
        valText:[],
        rule:false,
        dreamStep:[
            {id:1,text:'选择梦想'},
            {id:2,text:'诉说梦想'},
            {id:3,text:'实现梦想'},
            {id:4,text:'梦想加油站'},
        ],
        c:{}
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            _this.step=_this.c.getUrlParam('step');
            if(_this.step=='1'){
                $.getJSON(_this.c.Root+"newYear/getplans.json?data="+_this.token, function(e){
                    console.log(e)
                    if(e.status == 200){
                        _this.res = e.data;
                        $.loaded();
                        $("#Dream").show();
                        if(e.data.plan_count>0){
                            _this.$nextTick(function(){
                                var isdrag = false;
                                var disX;
                                var disY;
                                var Width = $("#historyPlan").width();
                                var Height = $("#historyPlan").height();
                                var NowLeft = $("#historyPlan").position().left;
                                var NowTop = $("#historyPlan").position().top;
                                var W = $(window).width();
                                var H = $(window).height();
                                var rW = W - Width;
                                var rH = H - Height;
    
                                var oDiv2 = document.getElementById("historyPlan");
                                oDiv2.addEventListener('touchstart', thismousedown);
                                oDiv2.addEventListener('touchend', thismouseup);
                                oDiv2.addEventListener('touchmove', thismousemove);
    
                                function thismousedown(e) {
                                    isdrag = true;
                                    NowLeft = $("#historyPlan").position().left;
                                    NowTop = $("#historyPlan").position().top;
                                    disX = e.touches[0].pageX;
                                    disY = e.touches[0].pageY;
                                    return false;
                                }
    
                                function thismousemove(e) {
                                    if (isdrag) {
                                        e.preventDefault();
                                        oDiv2.style.left = NowLeft + e.touches[0].pageX - disX + 'px';
                                        oDiv2.style.top = NowTop + e.touches[0].pageY - disY + 'px';
                                        if (NowLeft + e.touches[0].pageX - disX >= 0 && NowLeft + e.touches[0].pageX - disX <= rW) {
                                            oDiv2.style.left = oDiv2.style.left;
                                        } else if (NowLeft + e.touches[0].pageX - disX < 0) {
                                            oDiv2.style.left = 0
                                        } else if (NowLeft + e.touches[0].pageX - disX > rW) {
                                            oDiv2.style.left = rW + 'px';
                                        }
                                        if (NowTop + e.touches[0].pageY - disY >= 0 && NowTop + e.touches[0].pageY - disY <= rH) {
                                            oDiv2.style.top = oDiv2.style.top;
                                        } else if (NowTop + e.touches[0].pageY - disY < 0) {
                                            oDiv2.style.top = 0
                                        } else if (NowTop + e.touches[0].pageY - disY > rH) {
                                            oDiv2.style.top = rH + 'px';
                                        }
                                        return false;
                                    }
                                }
                                function thismouseup() {
                                    var lef = parseInt(oDiv2.style.left + 0);
                                    console.log(lef)
                                    if (lef < rW / 2 && lef>0) {
                                        oDiv2.style.left = 0
                                    } else {
                                        oDiv2.style.left = rW + 'px';
                                    }
                                    isdrag = false;
                                };
                            }); 
                            
                        }else{
                        }
                    }
                });
            }else if(_this.step=='2'){
                _this.title =sessionStorage.getItem('plan_title');
                if(_this.c.getUrlParam('status')==0){
                    _this.val=['','',''];
                    _this.$nextTick(function(){
                        $('.rule-bg').hide();
                        $('#activity_rule').hide();
                        $.loaded();
                        $("#Dream").show();
                        var windheight = $(window).height();  /*未唤起键盘时当前窗口高度*/        
                        $(window).resize(function(){
                            var docheight = $(window).height();  /*唤起键盘时当前窗口高度*/       
                            if(docheight < windheight){            /*当唤起键盘高度小于未唤起键盘高度时执行*/
                                _this.fou();
                            }else{
                                _this.fou();
                            }         
                        });
                    })
                    sessionStorage.setItem('user_plan_id',0);
                }else if(_this.c.getUrlParam('status')==1){
                    $.getJSON(_this.c.Root+"newYear/editplan.json?data="+_this.token+"&plan_id="+_this.c.getUrlParam('plan_id'), function(e){
                        _this.$nextTick(function(){
                            if(e.status == 200){
                                $.loaded();
                                $("#Dream").show();
                                _this.val.push(e.data.plan_years);
                                _this.val.push(e.data.myself_deposit);
                                _this.val.push(e.data.plan_money);
                                sessionStorage.setItem('user_plan_id',e.data.user_plan_id);
                                _this.val[0] = parseInt(_this.val[0])
                                _this.val[1] = parseInt(_this.val[1])
                                _this.val[2] = parseInt(_this.val[2])
                                $('.val0').val(_this.val[0]);
                                $('.val1').val(_this.val[1]);
                                $('.val2').val(_this.val[2]);
                                var windheight1 = $(window).height();  /*未唤起键盘时当前窗口高度*/        
                                $(window).resize(function(){
                                    var docheight1 = $(window).height();  /*唤起键盘时当前窗口高度*/       
                                    if(docheight1 < windheight1){            /*当唤起键盘高度小于未唤起键盘高度时执行*/
                                        _this.fou();
                                    }else{
                                        _this.fou();
                                    }         
                                });
                                _this.fou();
                            }
                        })
                    });
                }

            }else if(_this.step=='3'){
                var valText1 = sessionStorage.getItem('valText').split(',');
                var initData = {
                    data:_this.token,
                    plan_years:valText1[0],
                    myself_deposit:valText1[1],
                    plan_money:valText1[2]
                }
                console.log(typeof(initData))
                $.post(_this.c.Root+"newYear/compute.json",initData, function(e){
                    if(e.status == 200){
                        _this.title =sessionStorage.getItem('plan_title');
                        _this.res = e.data;
                        $.loaded();
                        $("#Dream").show();
                    }
                });
            }
            
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        Input:function(){
            var _this = this;
            _this.$nextTick(function(){
                if(_this.val[1]>0&&_this.val[0]>0&&_this.val[2]>0){
                    $('.next-btn img').attr('src','img/btn_xiayibu1.png')
                }else{
                    $('.next-btn img').attr('src','img/btn_xiayibu.png')
                }
            })
        },
        //失去焦点
        blur:function(val,num){
            console.log(typeof(this.val[1]))
            var _this = this;
            _this.$nextTick(function(){
                if(val==''){
                    val = ''
                }else{
                    val = parseInt(val)
                }
                _this.val[num] = val;
                $('.val'+num).val('');
                if(_this.val[0]==''){
                    _this.val[0]='';
                    $('.val0').css({'font-size':'42px'});
                }else{
                    $('.val0').css({'font-size':'53px'});
                }
                _this.fou()
                if(_this.val[0]>0&&_this.val[1]>=0&&_this.val[2]>=0&&typeof (_this.val[0])=='number'&&typeof (_this.val[1])=='number'&&typeof (_this.val[2])=='number'){
                    $('.next-btn img').attr('src','img/btn_xiayibu1.png')
                    $('.val0').css({'font-size':'53px'})
                    $('.val1').css({'font-size':'53px'})
                    $('.val2').css({'font-size':'53px'})
                }else{
                    $('.next-btn img').attr('src','img/btn_xiayibu.png')
                }
            })
        },
        //了解详情
        goRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('see');
                _this.rule=true;
            })
        },
        // 关闭规则
        closeRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.rule = false;
            })
        },
        //切换步骤2
        switchStep:function(statu,planId,sort){
            var _this = this;
            _this.$nextTick(function(){
                console.log(location.href.split('&step')[0]);
                sessionStorage.setItem('plan_title',_this.tit[sort-1]);
                sessionStorage.setItem('plan_id',planId);
                _this.stat('select');
                window.location.href=location.href.split('&step')[0]+'&step=2'+'&status='+statu+'&plan_id='+planId;
            })
        },
        // 下一步
        nextBtn:function(){
            var _this = this;
            _this.$nextTick(function(){
                if($('.next-btn img').attr('src')=='img/btn_xiayibu1.png'){
                    _this.valText.push(_this.val[0])
                    _this.valText.push(_this.val[1])
                    _this.valText.push(_this.val[2])
                    sessionStorage.setItem('valText',_this.valText);
                    _this.stat('next');
                    window.location.href=location.href.split('&step')[0]+'&step=3'
                }
            })
        },
        //历史计划
        historyPlan:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('his');
                window.location.href='plans.html?data='+_this.c.getUrlParam('data')
            })
        },
        // 为梦想加油
        comeOnbtn:function(){
            var _this = this;
            _this.$nextTick(function(){
                var user_plan_id=sessionStorage.getItem('user_plan_id');
                var plan_id=sessionStorage.getItem('plan_id');
                if(user_plan_id == null){
                    user_plan_id = 0;
                }
                var valText2 = sessionStorage.getItem('valText').split(',');
                var Data = {
                    data:_this.token,
                    user_plan_id:user_plan_id,
                    plan_id:plan_id,
                    plan_years:valText2[0],
                    myself_deposit:valText2[1],
                    plan_money:valText2[2],
                    amount:_this.res.amount
                }
                $.post(_this.c.Root+"newYear/addplan.json",Data, function(e){
                    if(e.status == 200){
                        _this.stat('come')
                        sessionStorage.setItem('offer_coupon',e.data.offer_coupon);
                        window.location.href='station.html?data='+_this.c.getUrlParam('data')
                    }
                });
            })
        },
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('dream',d);
        },
        fou:function(){
            var _this = this;
            _this.$nextTick(function(){
                if(_this.val[0]==''){
                    _this.val[0]='';
                    $('.val0').css({'font-size':'42px'});
                }else{
                    $('.val0').css({'font-size':'53px'});
                }
                if(_this.val[1]=='' && typeof(_this.val[1]) == 'number'){
                    $('.c1').hide();
                    $('.val1').css({'font-size':'53px'})
                }else if(_this.val[1]=='' && typeof(_this.val[1]) == 'string'){
                    $('.c1').show();
                    $('.val1').css({'font-size':'42px'})
                }
                if((_this.val[2]==''||_this.val[2]==0) && typeof(_this.val[2]) == 'number'){
                    $('.c2').hide();
                    $('.val2').css({'font-size':'53px'})
                }else if((_this.val[2]==''||_this.val[2]==0) && typeof(_this.val[2]) == 'string'){
                    $('.c2').show();
                    $('.val2').css({'font-size':'42px'})
                }
                console.log(typeof (_this.val[0]), typeof (_this.val[1]), _this.val[2])
                // if (typeof (_this.val[0])=='number'||typeof (_this.val[1])=='number'||typeof (_this.val[2])=='number'){
                    
                // }
                if(_this.val[0]>0&&_this.val[1]>=0&&_this.val[2]>=0&&typeof (_this.val[0])=='number'&&typeof (_this.val[1])=='number'&&typeof (_this.val[2])=='number'){
                    $('.next-btn img').attr('src','img/btn_xiayibu1.png')
                    $('.val0').css({'font-size':'53px'})
                    $('.val1').css({'font-size':'53px'})
                    $('.val2').css({'font-size':'53px'})
                }
            })
            
        }
    }
 });