      
 var app = new Vue({
    el:'#new_activity',
    data:{
        timer:0,
        timer1:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        res:{},
        bindBtn:0,
        rule:false,
        pop:false,
        // step:'1',
        c:{}
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.getJSON(_this.c.Root+"newYear/landing.json?data="+_this.token, function(e){
                $.loaded();
                $("#new_activity").show();
                if(e.status == 200){
                    _this.res = e.data;
                    
                    if(e.data.offer_coupon==1){
                        _this.pop = true;
                    }else{
                        _this.pop = false;
                    }
                    if(e.data.plan_count>0){
                        _this.$nextTick(function(){
                            var isdrag = false;
                            var disX;
                            var disY;
                            var Width = $("#historyPlan").width();
                            var Height = $("#historyPlan").height();
                            var NowLeft = $("#historyPlan").position().left;
                            var NowTop = $("#historyPlan").position().top;
                            var W = $(window).width();
                            var H = $(window).height();
                            var rW = W - Width;
                            var rH = H - Height;

                            var oDiv2 = document.getElementById("historyPlan");
                            oDiv2.addEventListener('touchstart', thismousedown);
                            oDiv2.addEventListener('touchend', thismouseup);
                            oDiv2.addEventListener('touchmove', thismousemove);

                            function thismousedown(e) {
                                isdrag = true;
                                NowLeft = $("#historyPlan").position().left;
                                NowTop = $("#historyPlan").position().top;
                                disX = e.touches[0].pageX;
                                disY = e.touches[0].pageY;
                                return false;
                            }

                            function thismousemove(e) {
                                if (isdrag) {
                                    e.preventDefault();
                                    oDiv2.style.left = NowLeft + e.touches[0].pageX - disX + 'px';
                                    oDiv2.style.top = NowTop + e.touches[0].pageY - disY + 'px';
                                    if (NowLeft + e.touches[0].pageX - disX >= 0 && NowLeft + e.touches[0].pageX - disX <= rW) {
                                        oDiv2.style.left = oDiv2.style.left;
                                    } else if (NowLeft + e.touches[0].pageX - disX < 0) {
                                        oDiv2.style.left = 0
                                    } else if (NowLeft + e.touches[0].pageX - disX > rW) {
                                        oDiv2.style.left = rW + 'px';
                                    }
                                    if (NowTop + e.touches[0].pageY - disY >= 0 && NowTop + e.touches[0].pageY - disY <= rH) {
                                        oDiv2.style.top = oDiv2.style.top;
                                    } else if (NowTop + e.touches[0].pageY - disY < 0) {
                                        oDiv2.style.top = 0
                                    } else if (NowTop + e.touches[0].pageY - disY > rH) {
                                        oDiv2.style.top = rH + 'px';
                                    }
                                    return false;
                                }
                            }
                            function thismouseup() {
                                var lef = parseInt(oDiv2.style.left + 0);
                                console.log(lef)
                                if (lef < rW / 2 && lef>0) {
                                    oDiv2.style.left = 0
                                } else {
                                    oDiv2.style.left = rW + 'px';
                                }
                                isdrag = false;
                            };
                        }); 
                        
                    }else{
                    }
                }
            });
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
            switch(key){
                case "goLogin":
                    _this.stat('gologin');
                    break;
                }
           return false;
        },
        //了解详情
        goRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('see');
                _this.rule=true;
                // $('.history-plan').hide()
            })
        },
        //述说梦想
        sayDream:function(){
            var _this = this;
            _this.$nextTick(function(){
                if(_this.isLogin == false) {
                    _this.c.goLogin();
                    return false;
                }else{
                    _this.stat('say');
                    window.location.href='dream.html?data='+_this.c.getUrlParam('data')+'&step=1'
                }
            })
        },
        // 立即领取
        getTicket:function(){
            var _this = this;
            _this.$nextTick(function(){
                $.getJSON(_this.c.Root+"newYear/getcoupon.json?data="+_this.token, function(e){
                    // $.loaded();
                    if(e.status == 200){
                        console.log("成功")
                        _this.stat('lq');
                        _this.pop=false
                    }
                });
            })
        },
        //历史计划
        historyPlan:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('his');
                window.location.href='plans.html?data='+_this.c.getUrlParam('data')
            })
        },
        // 关闭规则
        closeRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.rule = false;
            })
        },
        // 关闭pop
        closePop:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.pop = false;
            })
        },
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('dream',d);
        }

    }
 });
