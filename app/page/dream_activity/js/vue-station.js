      
 var app = new Vue({
    el:'#station',
    data:{
        timer:0,
        timer1:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:0,
        bindBtn:0,
        pop:false,
        // step:'1',
        c:{}
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            _this.$nextTick(function(){
                var offer_coupon = sessionStorage.getItem('offer_coupon');
                if(offer_coupon == 1){
                    _this.pop = true;
                }else{
                    _this.pop = false;
                }
                $.loaded();
                $("#station").show();
            })
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        addDream:function(index){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('add');
                window.location.href='dream.html?data='+_this.c.getUrlParam('data')+'&step=1'
            })
        },
        //弹窗
        goRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.pop=true;
            })
        },
        // 立即领取
        getTicket:function(){
            var _this = this;
            _this.$nextTick(function(){
                $.getJSON(_this.c.Root+"newYear/getcoupon.json?data="+_this.token, function(e){
                    // $.loaded();
                    if(e.status == 200){
                        console.log("成功")
                        _this.stat('lq');
                        _this.pop=false
                    }
                });
            })
        },
        //历史计划
        historyPlan:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.stat('his');
                window.location.href='plans.html?data='+_this.c.getUrlParam('data')
            })
        },
        // 关闭弹窗
        closeRule:function(){
            var _this = this;
            _this.$nextTick(function(){
                _this.pop = false;
            })
        },
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('dream',d);
        }
    }
 });