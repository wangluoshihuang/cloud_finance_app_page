
(function ($) {
    $.fn.extend({
        AutoSize: function () {
            var element = $(this);
            auto();
            function auto() {
                var width = $(window).width(),
                    height = $(window).height();
                $("html").css("font-size", width / 15);
                $(element).width(width).height(height);
            };
            $(window).resize(auto);
        }
    });
})(jQuery);

$(function () {
    $("body").AutoSize();
})

$(function () {
    var isdrag = false;
    var disX;
    var disY;
    var Width = $("#historyPlan").width();
    var Height = $("#historyPlan").height();
    // var NowLeft;
    // var NowTop;
    var NowLeft = $("#historyPlan").position().left;
    var NowTop = $("#historyPlan").position().top;
    var W = $(window).width();
    var H = $(window).height();
    var rW = W - Width;
    var rH = H - Height;

    var oDiv2 = document.getElementById("historyPlan");
    

    oDiv2.addEventListener('touchstart', thismousedown);
    oDiv2.addEventListener('touchend', thismouseup);
    oDiv2.addEventListener('touchmove', thismousemove);

    function thismousedown(e) {
        isdrag = true;
        NowLeft = $("#historyPlan").position().left;
        NowTop = $("#historyPlan").position().top;
        disX = e.touches[0].pageX;
        disY = e.touches[0].pageY;
        //  console.log(NowLeft,NowTop,disX,disY);
        return false;
    }

    function thismousemove(e) {
        if (isdrag) {
            e.preventDefault();
            // console.log(rW,Width)
            oDiv2.style.left = NowLeft + e.touches[0].pageX - disX + 'px';
            oDiv2.style.top = NowTop + e.touches[0].pageY - disY + 'px';
            console.log(NowLeft + e.touches[0].pageX - disX);
            if (NowLeft + e.touches[0].pageX - disX >= 0 && NowLeft + e.touches[0].pageX - disX <= rW) {
                oDiv2.style.left = oDiv2.style.left;
            } else if (NowLeft + e.touches[0].pageX - disX < 0) {
                oDiv2.style.left = 0
            } else if (NowLeft + e.touches[0].pageX - disX > rW) {
                oDiv2.style.left = rW + 'px';
            }
            if (NowTop + e.touches[0].pageY - disY >= 0 && NowTop + e.touches[0].pageY - disY <= rH) {
                oDiv2.style.top = oDiv2.style.top;
            } else if (NowTop + e.touches[0].pageY - disY < 0) {
                oDiv2.style.top = 0
            } else if (NowTop + e.touches[0].pageY - disY > rH) {
                oDiv2.style.top = rH + 'px';
            }
            return false;
        }
    }
    function thismouseup() {
        var lef = parseInt(oDiv2.style.left + 0);
        console.log(lef)
        if (lef < rW / 2 && lef>0) {
            oDiv2.style.left = 0
        } else {
            oDiv2.style.left = rW + 'px';
        }
        // console.log(oDiv2.style.left, rW / 2)
        isdrag = false;
    };
}); 