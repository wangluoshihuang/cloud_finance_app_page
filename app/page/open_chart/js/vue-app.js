var app = new Vue({
    el:'#channel_chart',
    
    data:{
        msg:"",
        list:[]
    },
    created: function () {
        $("#channel_chart").show();
        //this.init();   

    },
    methods: {
        //页面初始化
        init:function(){
           var c=new Cloud();
            if($('#startTime').val() == ''){
                alert('请选择起始日期');
                return false;
            }
            if($('#endTime').val() == ''){
                alert('请选择结束日期');
                return false;
            }
            var _this = this;
            var opt = {
                startTime:$('#startTime').val(),
                endTime:$('#endTime').val(),
                channelId: encodeURIComponent(c.getUrlParam2('data'))
            }
            _this.msg="正在查询，请稍后..."
            $.loading();
            $.ajax({
                url: c.manageRoot+"ydmgmt/api/public/getPlatformStatisByChannel",
                type: "get",
                data: opt,
                dataType:'json',
                success:function(data){
                   
                     $.loaded();
                     if(data.code == "0"){
                           _this.msg="查询成功";
                         _this.list= data.result;  
                     }else{
                           _this.msg=data.errorMsg;
                     }
                   
                },
                error:function(er){
                     $.loaded();
                      _this.msg="服务器繁忙，请稍后再试！";
                   
                }
            });
        }
    }
 });
