var app = new Vue({
    el: '#wmActivity',
    data: {
        apple: "*声明：此活动与苹果公司无关",
        token: 0,
        isLogin: false,
        res: {},
        pMoney: false,
        count: 0,
        c: {},
        amount: 0,
        record: 'false',
        ranks: {},
        tab: 1,
        rankList: [],
        rlist: [
            {
                username:null,
                amount:null
            },
            {
                username:null,
                amount:null
            },
            {
                username:null,
                amount:null
            }]
    },
    created: function () {
        var _this = this;
        if (window.mobileUtil.isAndroid) {
            _this.apple = "";
        }
        _this.c = new Cloud();
        _this.token = _this.c.getUrlParam('data');
        if (_this.token != undefined && _this.token != "") {
            _this.isLogin = true;
        }
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            
            $.loading();
            var dat = {
                token: _this.token
            }
            $.post(_this.c.ActivityRoot + "activities/spring-equinox", dat, function (e) {
                if (e.status_code == '200') {
                    console.log(200)
                    _this.res = e.data;
                    $.loaded();
                    if (e.data.award_record.length > 0) {
                        _this.record = 'true';
                    }
                    _this.$nextTick(function(){
                        $("#wmActivity").show();
                    })
                } else {
                    $.loaded();
                    _this.showMsg(e.msg);
                }
            });
            var datRank = {
                token: _this.token,
                page: 1,
                size: 10
            }
            $.post(_this.c.ActivityRoot + "activities/spring-ranking", datRank, function (e) {
                if (e.status_code == '200') {
                    console.log(202)
                    _this.ranks = e.data;
                    _this.rlist = e.data.ranking_list.slice(0, 3);
                    _this.rankList = e.data.ranking_list.slice(3, 10);
                    _this.$nextTick(function () {
                    })
                } else {
                    _this.showMsg(e.msg);
                }
            });
        },
        ling: function (obj, count) {
            var _this = this;
            var dat1 = {
                token: _this.token,
                id: 1,
                amount: 0
            }
            if (count == 'to') {
                dat1.id = obj.record_id;
                dat1.amount = obj.current_bonuses;
            } else if (count == 'pre') {
                dat1.id = obj.id;
                dat1.amount = obj.amount;
            }
            _this.amount = dat1.amount;
            $.post(_this.c.ActivityRoot + "activities/receive-bonus", dat1, function (e) {
                if (e.status_code == '200') {
                    _this.pMoney = true;
                    _this.init();
                } else if (e.code == '22003') {

                }
            });
            // $(".ling").eq(0).css({'transform':'scale(1.2)'});
            setTimeout(function () {

            }, 300)
        },
        closeMoney: function () {
            this.pMoney = false;
            $("#activity").css("position", "static");
            $("#activity").css("height", "auto");
            $("#activity").css("overflow", "auto");
        },
        //跳转到客户端
        goOn: function (key, opt) {
            var _this = this;
            if (_this.bindBtn != true) {
                return false;
            }
            if (_this.isLogin == false) {
                _this.stat('login');
                _this.c.goLogin();
                return false;
            } else {
                _this.stat('gopro');
            }
            _this.c.on(key, opt);
            return false;
        },
        stat: function (id) {
            var d = {};
            d[id] = 'true';
            MtaH5.clickStat('cf', d);
        },
        // 点击tab
        tab1: function () {
            var _this = this;
            if (_this.tab == 1) {
                return;
            }
            _this.tab = 1;
            $('ul.tab').removeClass('tab2');
            $('ul.tab').addClass('tab1');
            $('.wrap2').hide();
            $('.wrap1').show();
            // _this.$nextTick(function () {
            var dat = {
                token: _this.token
            }
            $.post(_this.c.ActivityRoot + "activities/spring-equinox", dat, function (e) {
                if (e.status_code == '200') {
                    _this.res = e.data;
                    if (e.data.award_record.length > 0) {
                        _this.record = 'true';
                    }

                } else {
                    _this.showMsg(e.msg);
                }
            });
        },
        tab2: function () {
            var _this = this;
            // _this.$nextTick(function () {
            if (_this.tab === 2) {
                return;
            }
            _this.tab = 2;
            $('ul.tab').removeClass('tab1');
            $('ul.tab').addClass('tab2');
            $('.wrap1').hide();
            $('.wrap2').show();
            var datRank = {
                token: _this.token,
                page: 1,
                size: 10
            }
            $.post(_this.c.ActivityRoot + "activities/spring-ranking", datRank, function (e) {
                if (e.status_code == '200') {
                    _this.ranks = e.data;
                    _this.rlist = e.data.ranking_list.slice(0, 3);
                    _this.rankList = e.data.ranking_list.slice(3, 10);
                } else {
                    _this.showMsg(e.msg);
                }
            });
        },
        goRank: function () {
            var _this = this;
            _this.$nextTick(function () {
                window.location.href = "rank.html?data=" + _this.token;
            })
        },
        showMsg: function (txt) {
            $.popupTxt("wmActivity", "<span class='show'>" + txt + "</span>", 2000);
        }
    }
});