var app = new Vue({
    el:'#rank',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        ranks:[],
        pMoney:false,
        count:0,
        c:{},
        amount:0,
        record:'false',
        pageindex:1,
        stop:false,
        s:0
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init();
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            
            $.loading();
            var datRank = {
                token: _this.token,
                page:_this.pageindex,
                size:25
            }
            $.post(_this.c.ActivityRoot + "activities/spring-ranking", datRank, function (e) {
                // $.post("https://apih5.yd-jr.cn/api/activities/spring-ranking", datRank, function (e) {
                if (e.status_code == '200') {
                    $.loaded();
                    _this.ranks = e.data.ranking_list;
                    $("#rank").show();
                    _this.$nextTick(function () {
                        _this.scroll()
                    })
                } else {
                    $.loaded();
                    _this.showMsg(e.msg);
                }
            });
        },
        scroll:function () {
            var _this = this;
            _this.$nextTick(function () {
                $('ul').scroll(function (event) {
                    var scrollTop;
                    var WindowT,DocumentT,T,WindowT2,T2;
                    var event = window.event || event;
                    WindowT = $(window).height()-$('ul').offset().top;
                    WindowT2 = $(window).height()-$('ul').offset().top;
                    DocumentT = $('li').height()*$('li').length;
                    T = DocumentT - WindowT;
                    scrollTop = $('ul').scrollTop();
                    if(_this.stop == true){
                        console.log(_this.pageindex)
                        return;
                    }
                    if(_this.s == 2){
                        console.log(_this.pageindex)
                        return;
                    }
                    if (scrollTop>=T && _this.stop==false){
                        _this.loadmore();
                    }
                });
             });
        },
        loadmore: function () {
            var _this = this;
            _this.s=2;
            _this.pageindex+=1;
            var datRank1 = {
                token: _this.token,
                page:_this.pageindex,
                size:25
            }
            $.post(_this.c.ActivityRoot + "activities/spring-ranking", datRank1, function (e) {
                // $.post("https://apih5.yd-jr.cn/api/activities/spring-ranking", datRank1, function (e) {
                
                if (e.status_code == '200') {
                    if(e.data.ranking_list.length<25){
                        $("li:last-child").html("没有更多了");
                        _this.stop = true;
                        return;
                    }else{
                        for(var i=0;i<=e.data.ranking_list.length-1;i++){
                            _this.ranks.push(e.data.ranking_list[i]);
                            _this.s=1;
                        }
                    }
                }else{
                    _this.showMsg(e.msg);
                }
            });

        },
        showMsg:function(txt){
            $.popupTxt("rank","<span class='show'>"+txt+"</span>",2000);
        }
    }
 });
