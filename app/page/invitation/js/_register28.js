$(function(){
    var checkTimer = 0;
    var purchase = new Cloud();
    var phone="";
    var xy=true;
    var moveStatus = null
    //1、流量宝 2、魔积分 3、小米 4、酷划锁屏
    $.loading();
    $.ajax({
        type: 'POST',
        url: purchase.Root+"user/channelTraffic.json",
        data: {channel:channel,mark:channelName},
        success:function(data){
            $.loaded();
            if(data.status == 1){
            }else{
                console.log(data.info)
            }
        }
    });
    $(".checked").click(function(){
        var data = $(this).attr("data");
        if(data == 'true'){
            xy = false;
        }else{
            xy = true;
        }
        $(".checked").show();
        $(this).hide();
    });
    $("#getChecked").click(function(){
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        if(checkTimer == 0){
            $.loading();
            $.ajax({
                type:"get",
                url:purchase.Root+"user/smsCodeNoSign.json?mobile="+phone,
                success:function(data){
                    $.loaded();
                    var t=60;
                    checkTimer=setInterval(function(){
                        if(t==0){
                            clearInterval(checkTimer);
                            checkTimer = 0;
                            $("#getChecked").html("获取验证码");
                        }else{
                            t--;
                            $("#getChecked").html(t+"s");
                        }
                    },1000);
                }
            });
        }
    });
    $("#submit").click(function(){
        var checked = $('#checked').val();
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        if(!xy){
            showMsg("请勾选服务协议");
            return false;
        }
        if(checked == ""||checked == null){
            $("#checked").focus();
            showMsg("请输入验证码");
            return false;
        }
        $.loading();
        $.ajax({
            type: 'POST',
            url: purchase.Root+"user/loginH5.json",
            data: {mobile:phone,mobileAuthCode:checked,channelType:channel,channelName:channelName},
            success:function(data){
                $.loaded();
                if(data.code == "0"){
                    clearInterval(checkTimer);
                    checkTimer = 0;
                    $("#getChecked").html("获取验证码");
                    window.location.href="down_28.html#rule-fittime"
                }else{
                    showMsg(data.errorMsg);
                    if(data.code=='21012'){
                        clearInterval(checkTimer);
                        checkTimer = 0;
                        $("#getChecked").html("获取验证码");
                    }
                }
            }
        });
    });
    $(".fittime-close").click(function(){
        $(".fittime-mask").hide();
    });
    $('.fittime-rule span').click(function(){
        $('.fittime-mask').show()
    })
    /*$('#download .span1').on('click',function(){
        if(window.mobileUtil.isAndroid && window.mobileUtil.isWeixin){
            $('#show').show()
            $('#bg').show()
            moveStatus = 1
        }else{
            purchase.download();
        }
    })
    $('#bg').on('touchmove',function(event){
        if(moveStatus==1){
            event.preventDefault();
        }
    })*/
    var fun =function(e){
        if(moveStatus==1){
            e.preventDefault();
            e.stopPropagation();
        }
    }
    document.addEventListener("touchmove",fun,false);
})
function showMsg(txt){
    $.popupTxt("form","<span class='show'>"+txt+"</span>",2000);
}
