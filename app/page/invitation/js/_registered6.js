$(function(){
    var checkTimer = 0;
    var purchase = new Cloud();
    var phone="";
    var xy=true;
    //1、流量宝 2、魔积分 3、小米 4、酷划锁屏
    $.loading();
    $.ajax({
        type: 'POST',
        url: purchase.Root+"user/channelTraffic.json",
        data: {channel:channel,mark:channelName},
        success:function(data){
            $.loaded();
            if(data.status == 1){
            }else{
                console.log(data.info)
            }
        }
    });
    $(".checked").click(function(){
        var data = $(this).attr("data");
        if(data == 'true'){
            xy = false;
        }else{
            xy = true;
        }
        $(".checked").show();
        $(this).hide();
    });
    $("#getChecked").click(function(){
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        if(checkTimer == 0){
            $.loading();
            $.ajax({
                type:"get",
                url:purchase.Root+"user/smsCodeNoSign.json?mobile="+phone,
                success:function(data){
                    $.loaded();
                    // if(data.result){
                    //     $('#checked').val(data.result);
                    // }
                    var t=60;
                    checkTimer=setInterval(function(){
                        if(t==0){
                            clearInterval(checkTimer);
                            checkTimer = 0;
                            $("#getChecked").html("获取验证码");
                        }else{
                            t--;
                            $("#getChecked").html(t+"s");
                        }
                    },1000);
                }
            });
        }
    });
    $("#submit").click(function(){
        var checked = $('#checked').val();
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        if(!xy){
            showMsg("请勾选服务协议");
            return false;
        }
        if(checked == ""||checked == null){
            $("#checked").focus();
            showMsg("请输入验证码");
            return false;
        }
        // window.location.href = "down_activity.html";
        $.loading();
        $.ajax({
            type: 'POST',
            url: purchase.Root+"user/loginH5.json",
            data: {mobile:phone,mobileAuthCode:checked,channelType:channel,channelName:channelName},
            success:function(data){
                $.loaded();
                if(data.code == "0"){
                    clearInterval(checkTimer);
                    checkTimer = 0;
                    $("#getChecked").html("获取验证码");
                    _taq.push({convert_id:"79519986826", event_type:"form"});
                   window.location.href = "down_activity.html";
                }else{
                    showMsg(data.errorMsg);
                }

            }
        });
    });
    $(".close").click(function(){
        $(".bar-footer").hide();
    });
})
function showMsg(txt){
    $.popupTxt("form","<span class='show'>"+txt+"</span>",2000);
}
