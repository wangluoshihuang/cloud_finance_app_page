$(function(){
    var purchase = new Cloud();
    var data = {
        iphoneSchema:purchase.iphoneSchema,
        androidSchema:purchase.androidSchema
    }
    if($("body").attr('id') == "down_activity"){
        var type=window.location.pathname.substring(window.location.pathname.indexOf("down"));
        // alert(type)
        //微信
        if(window.mobileUtil.isAndroid && window.mobileUtil.isWeixin){
            $("#bg").show();
            $("#show").show();
        }
        // purchase.download(type,1);
        $("#download").click(function(){
            purchase.download(type,1);
        });
    }else if($("body").attr('id') != "down_h5"){
        var type=window.location.pathname.substring(window.location.pathname.indexOf("down"));
        //微信
        if(window.mobileUtil.isAndroid && window.mobileUtil.isWeixin){
            $("#bg").show();
            $("#show").show();
        }else{//非微信浏览器
            var e = Date.now();
            if (window.mobileUtil.isIos) {
               top.location.href = data.iphoneSchema;
            }else if (window.mobileUtil.isAndroid) {
                top.location.href = data.androidSchema;
            }
            setTimeout(function () {
                if (Date.now() - e < 3000 + 100) {
                    window.location.href = 'down_h5.html?type='+type;
                }
            }, 3000);
        }
        $("#download").click(function(){
            purchase.download(type,1)
        });

    }else{
        var type = purchase.getUrlParam('type');
        $("#download").click(function(){
            purchase.download(type);
        });
    }
    $('#bg').bind("touchmove",function(e){
        e.preventDefault();
    });

})
