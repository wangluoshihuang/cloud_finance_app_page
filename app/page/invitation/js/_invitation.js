$(function(){
    var checkTimer = 0;
    var phone="";
    var checked = true;
    var purchase = new Cloud();
    var ydjrcode = purchase.getUrlParam('ydjrcode');//获取邀请人推荐码
    var token = purchase.getUrlParam('data');//获取邀请人推荐码
    if(token && token.length>1){
        window.location=window.location.pathname+"?ydjrcode="+ydjrcode;
        return false;
    }
    if(!(ydjrcode&& ydjrcode.length>1)){
        window.location ="down.html?channel=invitation";
        return false;
    }
    $.loading();
    $.ajax({
        url: purchase.Root+"invite/inviteUserInfo.json?invitation_code="+ydjrcode,
        type: "get",
        dataType:'json',
        success:function(data){
            $.loaded();
            if(data.status == 200){
                $("#userTitle").html(data.mobile+"送您");
                var src="img/inv/img_touxiang.png";
                if(data.avatar&&data.avatar!="" && data.avatar !=null){
                    src=data.avatar;
                }
                var day=data.savelyInvestmentDays||0;
                $("#userInfo").html("<img src='"+src+"'><p>好友"+data.mobile+"已在云端金融安全投资了"+day+"天，给你发来666元红包，邀请你一起来赚钱！注册立享16%往期平均收益率</p>");

            }else{
                window.location ="down.html?channel=invitation";
                return false;
            }
        },
        error:function(er){
             $.loaded();
           
        }
    });
    $(".checked").click(function(){
        var data = $(this).attr("data");
        if(data == 'true'){
            checked = false;
        }else{
            checked = true;
        }
        $(".checked").show();
        $(this).hide();
    });
    $("#getChecked").click(function(){
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        if(checkTimer == 0){
            $.loading();
            $.ajax({
                type:"get",
                url:purchase.Root+"user/smsCodeNoSign.json?mobile="+phone,
                success:function(data){
                    $.loaded();
                    // if(data.result){
                    //     $('#checked').val(data.result);
                    // }
                    
                    var t=60;
                    checkTimer=setInterval(function(){
                        if(t==0){
                            clearInterval(checkTimer);
                            checkTimer = 0;
                            $("#getChecked").html("获取验证码");
                        }else{
                            t--;
                            $("#getChecked").html(t+"s");
                        }
                    },1000);
                }
            });
        }
    });
    $("#submit").click(function(){
        MtaH5.clickStat('2',{'5':'true'});
        var getPhone = $("#phone").val();//获取受邀人手机号
        if(getPhone == ""||getPhone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!checked){
            showMsg("请勾选服务协议");
            return false;
        }
        if(!purchase.checkTel(getPhone)){
            $("#phone").focus();
            showMsg("请输入正确格式的手机号");
            return false;
        }
        var smsCode = $('#checked').val();
        if(smsCode == ""||smsCode == null){
            $("#checked").focus();
            showMsg("请输入验证码");
            return false;
        }
        $.loading();
        $.ajax({
            type:"get",
            url:purchase.Root+"user/inviteFrendNew.json?phone="+getPhone+"&invationCode="+ydjrcode+"&smsCode="+smsCode,
            success:function(data){
                $.loaded();
                if(data.code != '000'){
                    showMsg(data.result||'系统错误');
                }else{
                    window.location ="down.html?channel=invitation";
                }

            }
        });
    });
    function showMsg(txt){
        $.popupTxt("form","<span class='show'>"+txt+"</span>",2000);
    }
    $("#showE").click(function(){
        var h =$(window).height();
        window.scrollTo(h,h);
    });
    if($("body").attr('id') == "invit_share"){
        $.ajax({
            type:"get",
            url:purchase.Root+"breakfast/recordChance.json?&invitation_code="+ydjrcode,
            success:function(data){
            }
        });
    }
    
})
