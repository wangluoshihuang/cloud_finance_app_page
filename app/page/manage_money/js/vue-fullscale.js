var app = new Vue({
    el: '#fullscale',
    data: {
        pageindex: 1,
        fullscalelist: [],
        loaded:false,
        more:false,
        title:'',
        systemdata:null,
        Res:[],
        ss:0
    },
    created: function () {
        var _this = this;
        if (window.mobileUtil.isAndroid) {
            $('.main .header').css({
                'height': '127px'
            })
            $('#pullDown').css({
                'height':'127px',
                'line-height':'127px'
            })
            $('.systemheight').hide()
        } else{
            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                $('.main .header').css({
                    'height':'227px'
                })
                $('.systemheight').css({
                    'height':'100px'
                })
                $('.systemheight').css({
                    'height':'100px'
                })
                $('#pullDown').css({
                    'height':'227px',
                    'line-height':'227px'
                })
            }
            if(window.mobileUtil.isIos&&(screen.height == 568 && screen.width == 320)){
                $(".moneydetail").css("overflow",'hidden')
                $(".moneydetail div").css("display",'inline-block')
                $(".moneydetail").css("width",'100%')
                $(".moneydetail_fl").css("float",'left')
                $(".moneydetail_fr").css("float",'right')
                $('#pullDown').css({
                    'height':'184.6px',
                    'line-height':'227px'
                })
            }
        }
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            _this.title = _this.c.getUrlParam3('name');
            $("#fullscale").show();
            
            _this.initdata(1)
            _this.scroll();
            _this.touchdown();
        },
        initdata: function (pageindex) {
            var _this = this;
            $.loading();
            if (pageindex == 1) {
                _this.pageindex = pageindex
            }
            if( $(document).height() > $(window).height()){
                _this.more = true
            }
            _this.c.getparameter(function(systemdata,loginstatus){
                _this.systemdata = systemdata
                var time = _this.c.formatDate1()
                var password = '@?&]ydjr@123]}}' + time
                var md5password = hex_md5(password);
                _this.systemdata.pwd = md5password
                _this.systemdata.timestamp = Date.parse(new Date())
                _this.systemdata.pageNo = _this.pageindex
                _this.systemdata.status = _this.c.getUrlParam('status');
                // alert(_this.systemdata.status)
                $.post( _this.c.ActivityRoot +'project/moreProjectH5', _this.systemdata, function (e) {
                    if(e.status_code==200){
                        if (e.data.code == "0") {
                            e.data.result.forEach(function (i, ind) {
                                if(i.userRaiseInterest!='0'){
                                    i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                    i.userInterest=i.userInterest.toFixed(2)
                                    i.userRaiseInterest=i.userRaiseInterest*1.0;
                                    i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                }else if(i.userRaiseInterest=='0'){
                                    i.userInterest=i.userInterest*1.0;
                                    i.userInterest=i.userInterest.toFixed(2);
                                }
                            })
                            _this.fullscalelist = e.data.result;
                            $.loaded();
                            $("#wrapper").show();
                        } else {
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else {
                        _this.showMsg(e.msg);
                    }
                })
            })
        },
        touchdown: function () {
            var _this = this;
            _this.$nextTick(function () {
                var startY = 0;
                var endY = 0;
                var distanceY;
                var marginT = $('#wrapper').css("margin-top").split('px')[0];
                $("#wrapper").on("touchstart", function (e) {
                    var ev = window.event || e;
                    //获取第一个点
                    startY = ev.touches[0].pageY;
                })
                $("#wrapper").on("touchmove", function (e) {
                    var ev = window.event || e;
                    //获取最后一个点
                    endY = ev.touches[0].pageY;
                    distanceY = startY - endY;
                    if($(window).scrollTop()<=0){
                        if (distanceY <= -127) {
                            $('.pullDownLabel').html("松开刷新数据");
                            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                                $('#wrapper').css("margin-top", marginT + 227 + "px")
                            }else{
                                $('#wrapper').css("margin-top", marginT + 127 + "px")
                            }
                        } else if (distanceY <= 0 && distanceY > -127) {
                            $('.pullDownLabel').html("下拉刷新");
                            $('#wrapper').css("margin-top", marginT - distanceY + "px")
                        }
                    }
                })
                $("#wrapper").on("touchend", function () {
                    distanceY = startY - endY;
                    if (startY - endY > 10) {}
                    if (-distanceY > 0) {
                        $('.pullDownLabel').html("正在加载...");
                        $('#wrapper').animate({
                            'marginTop': marginT + "px"
                        },function(){
                            if($(window).scrollTop()<=0){
                                _this.loaded = false;
                                _this.ss = 0;
                                _this.initdata(1);
                            }
                        });
                    }
                })
            });
        },
        scroll:function () {
            var _this = this;
            _this.$nextTick(function () {
                $(window).scroll(function (event) {
                    var scrollTop;
                    var WindowT,DocumentT,T;
                    var event = window.event || event;
                    WindowT = $(window).height();
                    DocumentT = $(document).height();
                    T = DocumentT - WindowT;
                    scrollTop = $(window).scrollTop();
                    if(_this.ss==2){
                        return;
                    }
                    if (scrollTop > 0 &scrollTop <= T){
                        _this.loadmore();
                    }
                });
             });
        },
        loadmore: function () {
            var _this = this;
            _this.ss=2;
            _this.pageindex+=1;
            _this.c.getparameter(function(systemdata,loginstatus){
                 _this.systemdata = systemdata
                var time = _this.c.formatDate1()
                var password = '@?&]ydjr@123]}}' + time
                var md5password = hex_md5(password);
                _this.systemdata.pwd = md5password
                _this.systemdata.timestamp = Date.parse(new Date())
                _this.systemdata.pageNo = _this.pageindex
                _this.systemdata.status = _this.c.getUrlParam('status')
                $.post(_this.c.ActivityRoot + 'project/moreProjectH5', _this.systemdata, function (e) {
                    if(e.status_code==200){
                        if (e.data.code == "0") {
                            if(e.data.result.length==0){
                                _this.loaded = true
                                _this.more = false
                                 return false
                            }else if(e.data.result.length>0 &e.data.result.length<=10){
                                _this.$nextTick(function(){
                                    e.data.result.forEach(function (i, ind) {
                                        if(i.userRaiseInterest!='0'){
                                            i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                            i.userInterest=i.userInterest.toFixed(2)
                                            i.userRaiseInterest=i.userRaiseInterest*1.0;
                                            i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                        }else if(i.userRaiseInterest=='0'){
                                            i.userInterest=i.userInterest*1.0;
                                            i.userInterest=i.userInterest.toFixed(2);
                                        }
                                    })
                                    _this.fullscalelist = _this.fullscalelist.concat(e.data.result);
                                    _this.Res = _this.fullscalelist;
                                    _this.ss=1;
                                })
                            }
                        } else {
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else {
                        _this.showMsg(e.msg);
                    }
                })
            })

        },
        back:function(){
            var _this = this;
            _this.c.toFinish();
        },
        showMsg:function(txt){
                $.popupTxt("fullscale","<span class='show'>"+txt+"</span>",2000);
        }
    }
});