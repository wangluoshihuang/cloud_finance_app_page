var app = new Vue({
    el: '#product',
    data: {
        token: 0,
        time: "2018-2-28 17:23:23",
        startDate: [],
        res: {},
        startS: [],
        systemdata:null,
        loginstatus:null,
        image:null,
        dIntrest:null
    },
    created: function () {
        if (window.mobileUtil.isAndroid) {
            $('.main .header').css({
                'height': '127px'
            })
            $('.banner').css({
                'top': '127px'
            })
            $('#wrapper').css({
                'top': '360px'
            })
            $('.systemheight').hide()
        } else{
            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                $('.main .header').css({
                    'height':'227px'
                })
                $('.banner').css({
                    'top':'227px'
                })
                $('#wrapper').css({
                    'top':'460px'
                })
                $('.systemheight').css({
                    'height':'100px'
                })
                $('#thelist').css({
                    'padding-bottom':'78px'
                })
            }
            if(window.mobileUtil.isIos&&(screen.height == 568 && screen.width == 320)){
                $(".moneydetail").css("overflow",'hidden')
                $(".moneydetail div").css("display",'inline-block')
                $(".moneydetail").css("width",'100%')
                $(".moneydetail_fl").css("float",'left')
                $(".moneydetail_fr").css("float",'right')
            }
        }
        var _this = this;
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            var bannerdata={
                platform:'1',
                position:'3',
                action:'19'
            }
            $.post(_this.c.ActivityRoot+'message/h5BannerList', bannerdata, function (e) {
                if(e.status_code==200){
                    if (e.data.code == "0") {
                        _this.image = "https://cdn.yd-jr.cn/ydjr"+e.data.result[0].image;
                    } else {
                        _this.showMsg(e.data.errorMsg);
                    }
                }else {
                    _this.showMsg(e.msg);
                }
            })
            $("#product").show(); 
            // $("#wrapper").hide();
            _this.initdata()
            _this.touchdown();
        },
        initdata: function () {
            var _this = this;
            $.loading();
            _this.c.getparameter(function(systemdata,loginstatus){
                _this.systemdata = systemdata
                var time = _this.c.formatDate1()
                var password = '@?&]ydjr@123]}}' + time
                var md5password = hex_md5(password);
                _this.systemdata.pwd = md5password
                _this.systemdata.timestamp = Date.parse(new Date());
                $.post(_this.c.ActivityRoot+'project/queryInProgressProjectH5', _this.systemdata, function (e) {
                    if(e.status_code==200){
                        if (e.data.code == "0") {
                            _this.startS = e.data.result;
                            _this.res = e.data;
                            $.loaded();
                            $("#wrapper").fadeIn(600);
                            e.data.result.forEach(function (item, index) {
                                _this.startDate.push(item);
                                if (item.categoryName === '活动专区' || item.categoryName === '热销中'||item.categoryName === '个人专区') {
                                    // alert(item.spList.length)
                                    item.spList.forEach(function (i, ind) {
                                        // alert(i.userRaiseInterest)
                                        if(i.userRaiseInterest!='0'){
                                            i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                            i.userInterest=i.userInterest.toFixed(2)
                                            i.userRaiseInterest=i.userRaiseInterest*1.0;
                                            i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                        }else if(i.userRaiseInterest=='0'){
                                            i.userInterest=i.userInterest*1.0;
                                            i.userInterest=i.userInterest.toFixed(2);
                                        }
                                        var ttt = i.currentSystemTime - i.startTime;
                                        if (ttt > 0) {
                                            _this.startS[index].spList[ind].ss = true;
                                        }
                                        if (ttt <= 0) {
                                            _this.startS[index].spList[ind].ss = false;
                                            _this.timeing(i.currentSystemTime, i.startTime,index,ind);
                                        }
                                        _this.$nextTick(function () {
                                            $(".productcategory").eq(index).find('.productitem').eq(ind).find('.progessnum').css(
                                                { "width": i.percent + '%' })
                                        })
                                    })
                                }else{
                                    item.spList.forEach(function (i, ind) {
                                        if(i.userRaiseInterest!='0'){
                                            i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                            i.userInterest=i.userInterest.toFixed(2)
                                            i.userRaiseInterest=i.userRaiseInterest*1.0;
                                            i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                        }else if(i.userRaiseInterest=='0'){
                                            i.userInterest=i.userInterest*1.0;
                                            i.userInterest=i.userInterest.toFixed(2);
                                        }
                                    })
                                }
                            })
        
                        } else {
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else {
                        _this.showMsg(e.msg);
                    }
                })
           })
        },
        touchdown: function () {
            var _this = this;
            _this.$nextTick(function () {
                var startY = 0;
                var endY = 0;
                var distanceY;
                var height = $('.pullDown').css("height").split('px')[0];
                $(window).on("touchstart", function (e) {
                    var ev = window.event || e;
                    //获取第一个点
                    startY = ev.touches[0].pageY;
                })
                $(window).on("touchmove", function (e) {
                    var ev = window.event || e;
                    //获取最后一个点
                    endY = ev.touches[0].pageY;
                    distanceY = startY - endY;
                    if($('#wrapper').scrollTop()<=0){
                        if (distanceY <= -127) {
                            $('.pullDownLabel').html("松开刷新数据");
                            $('.pullDown').css({
                                "height":height + 127 + "px",
                                'z-index': -1
                            })
                        } else if (distanceY <= 0 && distanceY > -127) {
                            $('.pullDownLabel').html("下拉刷新");
                            $('.pullDown').css({
                                "height":height - distanceY + "px",
                                'z-index': -1
                            })
                        }
                    }
                })
                $(window).on("touchend", function () {
                    distanceY = startY - endY;
                    if($('#wrapper').scrollTop()<=0){
                        if (startY - endY > 10) { }
                        if (-distanceY > 0) {
                            $('.pullDownLabel').html("正在加载...");
                            $('.pullDown').animate({
                                'height': height + "px"
                            }, function () {
                                if ($('.pullDown').css("height").split('px')[0] == 0) {
                                    $('.pullDownLabel').html('');
                                    if ($('#wrapper').scrollTop() <= 0) {
                                        _this.initdata();
                                    }
                                }
                            });
                        }
                    }
                })
            });
        },
        more: function (item) {
            var _this = this;
            var urladdress = _this.c.localRoot + "manage_money/fullscale.html?name=" + item.categoryName + '&status=' + item.spList[0].status
            var encodeurl = encodeURI(urladdress)
            var detailurl = {
                url: encodeurl
            }
            _this.c.goproductdetail(detailurl);
        },
        productdetail: function (item, i) {
            var _this = this;
            _this.c.getparameter(function(systemdata,isloginstatus){
                if (isloginstatus == "true") {
                    var urladdress = _this.c.localRoot + "manage_money/pro_buy.html?name=" + i.title + '&id=' + i.id
                    var encodeurl = encodeURI(urladdress)
                    var detailurl = {
                        url: encodeurl
                    }
                    if (item == '已满标' || item == '已还款' || item == '募集失败') {
                        return false
                    } else {
                        //云端四号
                        if (i.type == 192) {
                            _this.c.goproductdetail(detailurl);
                        }
                        //非云端四号 
                        else if (i.type != 192) {
                            _this.c.goPro(i.id)
                        }
                    }
                } else {
                    _this.c.goLogin()
                }
            })
        },
        goFriend: function () {
            var _this = this;
            _this.c.getparameter(function(systemdata,isloginstatus){
                if (isloginstatus == "true") {
                    var urladdress1 = _this.c.localRoot + "invite_friend/index.html?data=" + systemdata.token;
                    var encodeurl1 = encodeURI(urladdress1);
                    var detailurl1 = {
                        url: encodeurl1
                    }
                    // window.location.href = encodeurl1;
                    _this.c.goFriend(detailurl1);
                } else {
                    _this.c.goLogin()
                }
            })
        },
        timeing: function (timeStamp1, timeStamp2,index,ind) {
            var _this = this;
            _this.$nextTick(function () {
                var distancetime = new Date(timeStamp2).getTime() - new Date(timeStamp1).getTime();
                // distancetime = new Date(timeStamp2).getTime() - new Date(timeStamp1).getTime()
                var time1 = setInterval(function () {
                    distancetime -= 1000;
                    // console.log(distancetime)
                    if (distancetime >= 0) {
                        console.log(999449)
                        //如果大于0.说明尚未到达截止时间              
                        var day = Math.floor(distancetime % 1000);
                        var sec = Math.floor(distancetime / 1000 % 60);
                        var min = Math.floor(distancetime / 1000 / 60 % 60);
                        var hour = Math.floor(distancetime / 1000 / 60 / 60 % 24);
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (min < 10) {
                            min = "0" + min;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }

                        var ddd = day + ":" + hour + ":" + min + ":" + sec;
                        _this.startDate[index].spList[ind].startTime=ddd;
                        return day + ":" + hour + ":" + min + ":" + sec;
                    } else {
                        //若否，就是已经到截止时间了
                        // return "已截止！"
                        clearInterval(time1);
                        _this.c.getparameter(function(systemdata,isloginstatus){
                            _this.systemdata = systemdata
                            var time = _this.c.formatDate1()
                            var password = '@?&]ydjr@123]}}' + time
                            var md5password = hex_md5(password);
                            _this.systemdata.pwd = md5password
                            _this.systemdata.timestamp = Date.parse(new Date())
                            $.post(_this.c.ActivityRoot+'project/queryInProgressProjectH5', _this.systemdata, function (e) {
                                if(e.status_code==200){
                                    if (e.data.code == "0") {
                                        _this.startS = e.result;
                                        e.data.result.forEach(function (item, index) {
                                            if (item.categoryName === '活动专区' || item.categoryName === '热销中'||item.categoryName === '个人专区') {
                                                item.spList.forEach(function (i, ind) {
                                                    if(i.userRaiseInterest!='0'){
                                                        i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                                        i.userInterest=i.userInterest.toFixed(2)
                                                        i.userRaiseInterest=i.userRaiseInterest*1.0;
                                                        i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                                    }else if(i.userRaiseInterest=='0'){
                                                        i.userInterest=i.userInterest*1.0;
                                                        i.userInterest=i.userInterest.toFixed(2);
                                                    }
                                                    var ttt1 = i.currentSystemTime - i.startTime;
                                                    if (ttt1 > 0) {
                                                        _this.startS[index].spList[ind].ss = true;
                                                    }
                                                    if (ttt1 <= 0) {
                                                        _this.startS[index].spList[ind].ss = false;
                                                    }
                                                })
                                            }else{
                                                item.spList.forEach(function (i, ind) {
                                                    if(i.userRaiseInterest!='0'){
                                                        i.userInterest = (i.userInterest-i.userRaiseInterest)*1.0;
                                                        i.userInterest=i.userInterest.toFixed(2)
                                                        i.userRaiseInterest=i.userRaiseInterest*1.0;
                                                        i.userRaiseInterest=i.userRaiseInterest.toFixed(2);
                                                    }else if(i.userRaiseInterest=='0'){
                                                        i.userInterest=i.userInterest*1.0;
                                                        i.userInterest=i.userInterest.toFixed(2);
                                                    }
                                                })
                                            }
                                        })
                                    } else {
                                        _this.showMsg(e.data.errorMsg);
                                    }
                                }else {
                                    _this.showMsg(e.msg);
                                }
                            })
                        })
                    }
                }, 1000)
            })
        },
        showMsg:function(txt){
            $.popupTxt("product","<span class='show'>"+txt+"</span>",2000);
        }
    }
});
