var app = new Vue({
    el: '#assetdetail',
    data: {
        detailobj: {},
        repaylist: [],
        deanbeanlist: [],
        fundlist: [],
        showrepayment: false,
        showfund: false,
        quarter: false,
        periods: false,
        systemdata: null

    },
    created: function () {
        if (window.mobileUtil.isAndroid) {
            $('.main .header').css({
                'height': '127px'
            })
            $('#wrapper').css({
                'top': '127px'
            })
            $('.systemheight').hide()
        } else {
            if (window.mobileUtil.isIos && (screen.height == 812 && screen.width == 375)) {
                $('.main .header').css({
                    'height': '227px'
                })
                $('#wrapper').css({
                    'top': '227px'
                })
                $('.systemheight').css({
                    'height': '100px'
                })
            }
        }
        var _this = this;
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    filters: {
        formattime: function (value) {
            return value.replace(/\//g, ".")
        }
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            // $.loading();
            _this.initdata();
            _this.touchdown();
        },
        initdata: function () {
            var _this = this;
            _this.c.getparameter(function (systemdata, loginstatus) {
                _this.systemdata = systemdata
                var time = _this.c.formatDate1()
                var password = '@?&]ydjr@123]}}' + time
                var md5password = hex_md5(password);
                _this.systemdata.pwd = md5password
                _this.systemdata.timestamp = Date.parse(new Date())
                _this.systemdata.investDetailId = _this.c.getUrlParam('assetdetailid')
                _this.systemdata.projectId = _this.c.getUrlParam('projectId')
                $.post(_this.c.ActivityRoot + 'user/dueDetailH5', _this.systemdata, function (e) {
                    // $.loaded()
                    if(e.status_code == 200){
                        if (e.data.code == "0") {
                            _this.detailobj = e.data.result
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                        } else {
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else{
                        _this.showMsg(e.msg);
                    }
                })
                $.post(_this.c.ActivityRoot + 'user/duePlanDetail', _this.systemdata, function (e) {
                    // $.loaded()
                    if(e.status_code == 200){
                        if (e.data.code == "0") {
                            _this.repaylist = e.data.result;
                            
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                            if (_this.repaylist[0].numberCode.indexOf('季度') > -1) {
                                _this.quarter = true
                            }
                            if (_this.repaylist[0].numberCode.indexOf('期') > -1) {
                                _this.periods = true
                            }
                        } else {
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else{
                        _this.showMsg(e.msg);
                    }
                })
                $.post(_this.c.ActivityRoot + 'project/getCapitalInquire', _this.systemdata, function (e) {
                    if(e.status_code == 200){
                        if (e.data.code == "0") {
                            _this.fundlist = e.data.result
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                        } else {
                            $('#assetdetail').css({
                                'visibility': 'visible'
                            })
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else{
                        _this.showMsg(e.msg);
                    }
                })
            })
        },
        back: function () {
            var _this = this;
            _this.c.toFinish();
        },
        touchdown: function () {
            var _this = this;
            _this.$nextTick(function () {
                var startY = 0;
                var endY = 0;
                var distanceY;
                var height = $('.pullDown').css("height").split('px')[0];
                $(window).on("touchstart", function (e) {
                    var ev = window.event || e;
                    //获取第一个点
                    startY = ev.touches[0].pageY;
                })
                $(window).on("touchmove", function (e) {
                    var ev = window.event || e;
                    //获取最后一个点
                    endY = ev.touches[0].pageY;
                    distanceY = startY - endY;
                    if (distanceY <= -127) {
                        $('.pullDownLabel').html("松开刷新数据");
                        $('.pullDown').css("height", height + 127 + "px")
                    } else if (distanceY <= 0 && distanceY > -127) {
                        $('.pullDownLabel').html("下拉刷新");
                        $('.pullDown').css("height", height - distanceY + "px")
                    }
                })
                $(window).on("touchend", function () {
                    distanceY = startY - endY;
                    if (startY - endY > 10) { }
                    if (-distanceY > 0) {
                        $('.pullDownLabel').html("正在加载...");
                        $('.pullDown').animate({
                            'height': height + "px"
                        }, function () {
                            if ($('.pullDown').css("height").split('px')[0] == 0) {
                                $('.pullDownLabel').html('');
                            }
                        });
                        if ($('#wrapper').scrollTop() <= 0) {
                            _this.initdata(1);
                        }
                    }
                })
            });
        },
        showcontent1: function () {
            if (this.showrepayment) {
                $('.showrepayment').css({
                    'transition': '0.5s',
                    'transform': 'rotateZ(0deg)'
                })
                this.showrepayment = false

            } else {
                this.showrepayment = true
                $('.showrepayment').css({
                    'transition': '0.5s',
                    'transform': 'rotateZ(90deg)'
                })
            }
        },
        showcontent2: function () {
            if (this.showfund) {
                $('.showfund').css({
                    'transition': '0.5s',
                    'transform': 'rotateZ(0deg)'
                })
                this.showfund = false

            } else {
                this.showfund = true
                $('.showfund').css({
                    'transition': '0.5s',
                    'transform': 'rotateZ(90deg)'
                })
            }
        },
        proAgreement: function () {
            var _this = this;
            _this.$nextTick(function () {
                var urladdress = _this.c.localRoot + "manage_money/pro.html?name=购买合同&did="+_this.detailobj.id+"&pid="+_this.detailobj.projectId;
                var encodeurl = encodeURI(urladdress);
                var detailurl1 = {
                    url: encodeurl
                }
                _this.c.goproductdetail(detailurl1);
                
            })
        },
        showMsg: function (txt) {
            $.popupTxt("assetdetail", "<span class='show'>" + txt + "</span>", 2000);
        }
    }
});