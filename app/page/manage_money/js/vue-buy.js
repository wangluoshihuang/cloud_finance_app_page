var app = new Vue({
    el: '#pro_buy',
    data: {
        token: 0,
        isLogin: false,
        count: 1,
        width: '',
        raiseDate: [],
        res: {},
        c: {},
        preNum: 10,
        startDate: '立即投资',
        endDate: 0,
        start:{
            day:0,
            hour:0,
            min:0,
            sec:0
        },
        title:'',
        loandata:null,
        systemdata:null,
        startS:null,
        isflag:true
    },
    created: function () {
        var _this = this;
        if (window.mobileUtil.isAndroid) {
            $('.main .header').css({
                'height': '127px'
            })
            $('#wrapper').css({
                'top':'0px'
            })
            $('#pullDown').css({
                'height':'127px',
                'line-height':'127px'
            })
            $('.systemheight').hide()
        } else{
            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                $('.main .header').css({
                    'height':'227px'
                })
                $('.systemheight').css({
                    'height':'100px'
                })
                $('.systemheight').css({
                    'height':'100px'
                })
                $('#wrapper').css({
                    'top':'0px'
                })
                $('#pullDown').css({
                    'height':'227px',
                    'line-height':'227px'
                })
                
            }
        }
        
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            _this.title = _this.c.getUrlParam3('name');
            $("#pro_buy").show();
            // $.loading();
            _this.initData();
            _this.touch();
            
        },
        initData: function () {
            var _this = this;
            _this.$nextTick(function () {
                _this.c.getparameter(function(systemdata,loginstatus){
                    _this.systemdata = systemdata
                    var time = _this.c.formatDate1()
                    var password = '@?&]ydjr@123]}}' + time
                    var md5password = hex_md5(password);
                    _this.systemdata.pwd = md5password
                    _this.systemdata.timestamp = Date.parse(new Date())
                    _this.systemdata.projectId = _this.c.getUrlParam('id')
                    $.post(_this.c.ActivityRoot+'project/detailH5',  _this.systemdata, function (e) {
                        if(e.status_code==200){
                            if (e.data.code == "0") {
                                _this.loandata = e.data.result;
                                _this.raiseDate.push(e.data.result.raiseStartTime);
                                _this.raiseDate.push(e.data.result.raiseEndTime);
                                var raiseTime = [];
                                for (var i = 0; i <= _this.raiseDate.length - 1; i++) {
                                    var da = new Date(_this.raiseDate[i]);
                                    var year = da.getFullYear();
                                    var month = da.getMonth() + 1;
                                    var date = da.getDate();
                                    if (month <= 9) {
                                        month = '0' + month
                                    }
                                    if (date <= 9) {
                                        date = '0' + date
                                    }
                                    raiseTime.push([year, month, date].join('.'));
                                }
                                e.data.result.raiseStartTime = raiseTime[0];
                                e.data.result.raiseEndTime = raiseTime[1];
                                e.data.result.amount = e.data.result.amount / 10000;
                                e.data.result.userInterest = e.data.result.userInterest*1.0;
                                e.data.result.userInterest = e.data.result.userInterest.toFixed(2);
                                _this.res = e.data.result;
                                $(".bj_grey .active").css({ 'width': e.data.result.percent + "%" });
                                // $.loaded();
                                $("#wrapper").show();
                                var ttt = e.data.result.currentSystemTime - e.data.result.startTime;
                                if (ttt > 0) {
                                    _this.startS = true;
                                }
                                if (ttt <= 0) {
                                    _this.startS = false;
                                    _this.timeing(e.data.result.currentSystemTime, e.data.result.startTime)
                                }
                            } else {
                                _this.showMsg(e.data.errorMsg);
                            }
                        }else {
                            _this.showMsg(e.msg);
                        }
                    })
               })
            });
        },
        back:function(){
           var _this = this;
           _this.c.toFinish();
        },
        touch: function () {
            var _this = this;
            _this.$nextTick(function () {
                var startY = 0;
                var endY = 0;
                var distanceY;
                var marginT = $('#wrapper').css("margin-top").split('px')[0];
                $(window).on("touchstart", function (e) {
                    var ev = window.event || e;
                    //获取第一个点
                    startY = ev.touches[0].pageY;
                })
                $(window).on("touchmove", function (e) {
                    var ev = window.event || e;
                    //获取最后一个点
                    endY = ev.touches[0].pageY;
                    distanceY = startY - endY;
                    // console.log(marginT)
                    if($(window).scrollTop()<=0){
                        if (distanceY <= -127) {
                            $('.pullDownLabel').html("松开刷新数据");
                            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                                $('#wrapper').css("margin-top", marginT + 227 + "px")
                            }else{
                                $('#wrapper').css("margin-top", marginT + 127 + "px")
                            }
                        } else if (distanceY <= 0 && distanceY > -127) {
                            $('.pullDownLabel').html("下拉刷新");
                            $('#wrapper').css("margin-top", marginT - distanceY + "px")
                        }
                    }
                })
                $(window).on("touchend", function () {
                    distanceY = startY - endY;
                    if (startY - endY > 10) {}
                    if (-distanceY > 0) {
                        $('.pullDownLabel').html("正在加载...");
                        $('#wrapper').animate({
                            'marginTop': marginT + "px"
                        },function(){
                            if($(window).scrollTop()<=0){
                                _this.initData();
                            }
                        });
                    }
                })
            });
        },
        timeing: function (timeStamp1, timeStamp2) {
            var _this = this;
            _this.$nextTick(function () {
                var distancetime = new Date(timeStamp2).getTime() - new Date(timeStamp1).getTime();
                var time1 = setInterval(function () {
                    distancetime -= 1000;
                    if (distancetime > 0) {
                        //如果大于0.说明尚未到达截止时间              
                        var day = Math.floor(distancetime % 1000);
                        var sec = Math.floor(distancetime / 1000 % 60);
                        var min = Math.floor(distancetime / 1000 / 60 % 60);
                        var hour = Math.floor(distancetime / 1000 / 60 / 60 % 24);
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (min < 10) {
                            min = "0" + min;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        _this.startDate =day+"天"+ hour + "小时" + min + "分" + sec + "秒";
                        return hour + ":" + min + ":" + sec + ":" + day;
                    } else {
                        //若否，就是已经到截止时间了
                        // return "已截止！"
                        clearInterval(time1);
                        _this.initData();
                    }
                }, 1000)
            })
        },
        proTo:function(){
            var _this = this;
            _this.$nextTick(function(){
                var urladdress1 = _this.c.localRoot + "manage_money/pro.html?name=项目介绍&id="+_this.res.id;
                var encodeurl1 = encodeURI(urladdress1);
                var detailurl1 = {
                    url: encodeurl1
                }
                _this.c.goprodetail(detailurl1);
            })
        },
        proFrom:function(){
            var _this = this;
            _this.$nextTick(function(){
                var urladdress1 = _this.c.localRoot + "manage_money/pro.html?name=还款来源&id="+_this.res.id;
                var encodeurl1 = encodeURI(urladdress1);
                var detailurl1 = {
                    url: encodeurl1
                }
                _this.c.goprodetail(detailurl1);
            })
        },
        proNum:function(){
            var _this = this;
            _this.$nextTick(function(){
                var id = {
                    id:_this.res.id
                }
                _this.c.buylist(id);
            })
        },
        //跳转到客户端
        goOn: function () {
            var _this = this;
            if (_this.bindBtn != true) {
                return false;
            }
            _this.c.getparameter(function(systemdata,loginstatus){
                _this.systemdata = systemdata
                var time = _this.c.formatDate1()
                var password = '@?&]ydjr@123]}}' + time
                var md5password = hex_md5(password);
                _this.systemdata.pwd = md5password
                _this.systemdata.timestamp = Date.parse(new Date())
                _this.systemdata.pId = _this.c.getUrlParam('id')
                $.post(_this.c.ActivityRoot+'message/project/buyrightH5',  _this.systemdata, function (e) {
                    if(e.status_code==200){
                        if (e.data.code == "0") {
                            if(_this.isflag == true){
                                _this.isflag = false
                                _this.c.goloan(_this.loandata)
                                setTimeout(function(){
                                    _this.isflag = true
                                },2000)
                            }
                        } else {
                            _this.showMsg(e.data.errorMsg);
                        }
                    }else{
                        _this.showMsg(e.msg);
                    }
                })
           })
        },
        showMsg:function(txt){
                $.popupTxt("pro_buy","<span class='show'>"+txt+"</span>",2000);
        }
    }
});