var app = new Vue({
    el: '#pro_buy',
    data: {
        apple: "*声明：此活动与苹果公司无关",
        token: 0,
        isLogin: false,
        res: {},
        c: {},
        title:'',
        id:'',
        sign:null,
        systemdata:null
    },
    created: function () {
        var _this = this;
        if (window.mobileUtil.isIos) {
            $(".main").css("paddingTop","64px");
            $(".header").css("paddingTop","20px");
            $(".header").css("height","44px");
            if(window.mobileUtil.isIos&&(screen.height == 812 && screen.width == 375)){
                $(".main").css("paddingTop","85px");
                $(".header").css("paddingTop","40px");
                $(".header").css("height","44px");
            }
        } else{
            
        }
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            _this.$nextTick(function(){
                $("#pro_buy").show();
            })
            _this.title = _this.c.getUrlParam3('name');
            _this.c.getparameter(function(systemdata,loginstatus){
                _this.systemdata = systemdata;
                _this.sign =hex_md5(_this.c.getUrlParam('pid')+_this.systemdata.userId+_this.c.getUrlParam('did')+'zHQGfyGky2qO8GcDlq2KjHiaphYU1Ukc');
                _this.initData();
           })
            
        },
        initData: function () {
            var _this = this;
            _this.$nextTick(function () {
                if(_this.title=="项目介绍"){
                    $("#wrapper").load(_this.c.AdminRoot+'stone_admin2015.php/mobile/projectdescr/id/'+_this.c.getUrlParam('id')+'/target/1',function(e){
                    })
                }else if(_this.title=="还款来源"){
                    $("#wrapper").load(_this.c.AdminRoot+'stone_admin2015.php/mobile/projectdescr/id/'+_this.c.getUrlParam('id')+'/target/2',function(e){
                    })
                }else if(_this.title=="购买合同"){
                    _this.title="";
                    $("#wrapper").load(_this.c.AdminRoot+'stone_admin2015.php/mobile/contract/pid/'+_this.c.getUrlParam('pid')+'/uid/'+_this.systemdata.userId+'/did/'+_this.c.getUrlParam('did')+'/sign/'+_this.sign+'?data='+_this.systemdata.token,function(e){
                        _this.title=$(".content .list .title").html();
                        $(".content .list .title").css('paddingTop',"20px")
                        $(".content .list .title").css('marginBottom',"20px")
                    });
                }
            });
        },
        back:function(){
            var _this = this;
            _this.c.toFinish();
         },
        showMsg:function(txt){
                $.popupTxt("pro_buy","<span class='show'>"+txt+"</span>",2000);
        }
    }
});