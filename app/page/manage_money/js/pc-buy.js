var app = new Vue({
    el: '#pro_buy',
    data: {
        token: 0,
        isLogin: false,
        count: 1,
        width: '',
        raiseDate: [],
        res: {},
        c: {},
        preNum: 10,
        startDate: '立即出借',
        endDate: 0,
        start: {
            day: 0,
            hour: 0,
            min: 0,
            sec: 0
        },
        title: '',
        loandata: null,
        systemdata: null,
        startS: null
    },
    created: function () {
        var _this = this;
        _this.c = new Cloud();
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            _this.title = _this.c.getUrlParam3('name');
            $("#pro_buy").show();
            // $.loading();
            _this.initData();

        },
        initData: function () {
            var _this = this;
            _this.$nextTick(function () {
                var dddd = {
                    projectId: 1
                }
                dddd.projectId = _this.c.getUrlParam('id');
                $.post(_this.c.ActivityRoot + 'project/detailV3', dddd, function (e) {
                    if (e.data.code == "0") {
                        _this.loandata = e.data.result;
                        _this.raiseDate.push(e.data.result.raiseStartTime);
                        _this.raiseDate.push(e.data.result.raiseEndTime);
                        var raiseTime = [];
                        for (var i = 0; i <= _this.raiseDate.length - 1; i++) {
                            var da = new Date(_this.raiseDate[i]);
                            var year = da.getFullYear();
                            var month = da.getMonth() + 1;
                            var date = da.getDate();
                            if (month <= 9) {
                                month = '0' + month
                            }
                            if (date <= 9) {
                                date = '0' + date
                            }
                            raiseTime.push([year, month, date].join('.'));
                        }
                        e.data.result.raiseStartTime = raiseTime[0];
                        e.data.result.raiseEndTime = raiseTime[1];
                        e.data.result.amount = e.data.result.amount / 10000;
                        _this.res = e.data.result;
                        $(".bj_grey .active").css({ 'width': e.data.result.percent + "%" });
                        // $.loaded();
                        $("#wrapper").show();
                        var ttt = e.data.result.currentSystemTime - e.data.result.startTime;
                        if (ttt > 0) {
                            _this.startS = true;
                        }
                        if (ttt <= 0) {
                            _this.startS = false;
                            _this.timeing(e.data.result.currentSystemTime, e.data.result.startTime)
                        }
                    } else {
                        _this.showMsg(e.data.errorMsg);
                    }
                })
            });
        },
        back: function () {
            var _this = this;
            _this.c.toFinish();
        },
        timeing: function (timeStamp1, timeStamp2) {
            var _this = this;
            _this.$nextTick(function () {
                var distancetime = new Date(timeStamp2).getTime() - new Date(timeStamp1).getTime();
                var time1 = setInterval(function () {
                    distancetime -= 1000;
                    if (distancetime > 0) {
                        //如果大于0.说明尚未到达截止时间              
                        var day = Math.floor(distancetime % 1000);
                        var sec = Math.floor(distancetime / 1000 % 60);
                        var min = Math.floor(distancetime / 1000 / 60 % 60);
                        var hour = Math.floor(distancetime / 1000 / 60 / 60 % 24);
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (sec < 10) {
                            sec = "0" + sec;
                        }
                        if (min < 10) {
                            min = "0" + min;
                        }
                        if (hour < 10) {
                            hour = "0" + hour;
                        }
                        _this.startDate = day + "天" + hour + "小时" + min + "分" + sec + "秒";
                        return hour + ":" + min + ":" + sec + ":" + day;
                    } else {
                        //若否，就是已经到截止时间了
                        // return "已截止！"
                        clearInterval(time1);
                        _this.initData();
                    }
                }, 1000)
            })
        },
        proTo: function () {
            var _this = this;
            _this.$nextTick(function () {
                window.location.href = "../invitation/down.html";
            })
        },
        showMsg: function (txt) {
            $.popupTxt("pro_buy", "<span class='show'>" + txt + "</span>", 2000);
        }
    }
});