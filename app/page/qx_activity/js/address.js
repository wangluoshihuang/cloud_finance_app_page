var app = new Vue({
    el:'#address',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:{province:""},

        c:{}
    },
    created: function () {
        $("#address").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init();   
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: _this.c.Root+"qixi/reciveInfo.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                   _this.result = data; 
                   _this.result['data']=_this.token;
                   var p = (data.province == '')?'选择省':data.province;
                   var c= (data.city == '')?'选择省':data.city;
                   var d = (data.town == '')?'选择省':data.town;
                   $('#distpicker').distpicker({
                    placeholder: false,
                    province: p,
                    city: c,
                    district: d
                  });  
                 
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        changeAdd:function(e){
          var _this = this;
          var d="";
          setTimeout(function(){
            if($("#district option").size() == 0){
              $("#district").hide();
              $(".town").hide();
            }else{
               $("#district").show();
               $(".town").show();
               d = $("#district option:checked").text();
            }
             var p = $("#province option:checked").text();
             var c = $("#city option:checked").text();
             _this.result.province= (p == '选择省') ?'':p;
             _this.result.city=  (c == '选择市') ?'':c;
             _this.result.town=  (d == '选择区') ?'':d;
          },50);
        },
        submit:function(){
        
          var _this = this;
          $.loading();
            $.ajax({
                url: _this.c.Root+"qixi/editReciveInfo.json?data="+_this.token,
                type: "post",
                data: _this.result,
                dataType:'json',
                success:function(data){
                    $.loaded();
                      if(data.status == 200){
                        window.location.href = _this.c.pageRoot+"com/page/qx_activity/index.html?data=" + _this.token ;
                      }else{
                        var opt={
                            title:data.msg
                        };
                        $.openPop(opt);
                      }
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        }
    }
 });
