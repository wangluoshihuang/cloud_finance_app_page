var app = new Vue({
    el:'#qxActivity',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        timer1:0,
        timer2:0,
        result:{},
        prizeRecords:[],
        page:{isRule:false},
        l1:false,
        l2:false,
        l3:false,
        c:{}
    },
    created: function () {
        $("#qxActivity").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.slide();  
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: _this.c.Root+"qixi/index.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                   _this.result = data;
                   _this.prizeRecord(); 
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        },
        prizeRecord:function(){
            var _this=this;
            run();
            window.clearInterval(_this.timer1);
            _this.timer1=setInterval(function(){
               run()
            },10000);
            function run(){
                $.ajax({
                    url: _this.c.Root+"qixi/getCachePrizeList.json?data="+_this.token,
                    type: "get",
                    dataType:'json',
                    success:function(data){
                        _this.prizeRecords=data;
                        // for(var i=0;i<data.length;i++){
                        //     _this.prizeRecords.push(data[i]);
                        //     if(_this.prizeRecords.length==4){
                        //         _this.prizeRecords.splice(0,1);
                        //     }
                            
                        // }
                    },
                    error:function(er){
                        $.loaded();
                    }
                });

            }
        },
        slide:function(){
            var _this=this;
            clearTimeout(_this.timer2);
            
            _this.timer2=setTimeout(function(){
                _this.l1=true;
                 _this.timer2=setTimeout(function(){
                    _this.l2=true;
                     _this.timer2=setTimeout(function(){
                        _this.l3=true;

                   },1000);
               },1000);
           },1000);
             
        },
        openRule:function(){
             this.page.isRule = true;
             $("#activity").css("position","fixed");
             $("#activity").css("top",0);
             $("#activity").css("height",1749);
             $("#activity").css("overflow","hidden");
        },
        closeRule:function(){
            this.page.isRule = false;
            $("#activity").css("position","static");
            $("#activity").css("height","auto");
            $("#activity").css("overflow","auto");
        },
        goChannel:function(){
             window.location.href = this.c.pageRoot+"build/html/invitation/registered_5.html";
        },
        goAddress:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            window.location.href = _this.c.pageRoot+"com/page/qx_activity/address.html?data=" + _this.token;
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        }
    }
 });
