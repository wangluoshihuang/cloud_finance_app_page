$(function(){
    var checkTimer = 0;
    var phone="";
    var checked = true;
    var purchase = new Cloud();
    var ydjrcode = purchase.getUrlParam('ydjrcode');//获取邀请人推荐码
    var token = purchase.getUrlParam('data');//获取邀请人token
    var phone1 = purchase.getUrlParam('ydjrphone');//获取邀请人phone
    var time = purchase.getUrlParam('ydjraddtime');//获取邀请人day
    $.loading();
    var sDate = new Date();
    var stime = sDate.getTime();
    var stime1 = stime-time;
    var d = stime1/1000/3600/24
    var day = parseInt(d)
    // console.log(stime)
    // console.log(stime1)
    // console.log(d)
    // console.log(day)
    // console.log(time)
    if(day<=1){
        day = 1;
    }
    if(time == null || time == undefined || time == ''){
        day = 1;
    }
    if(phone1 == null || phone1 == undefined || phone1 == ''){
        phone1='156****1212';
    }
    $(".msg").html("您的好友"+phone1+"已安全投资"+day+"天，邀您加入云端金融");
    $.loaded();
    $(".checked").click(function(){
        var data = $(this).attr("data");
        if(data == 'true'){
            checked = false;
        }else{
            checked = true;
        }
        $(".checked").show();
        $(this).hide();
    });
    $("#getChecked").click(function(){
        phone=$("#phone").val();
        if(phone == ""||phone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!purchase.checkTel(phone)){
            $("#phone").focus();
            showMsg("请输入正确手机号");
            return false;
        }
        if(checkTimer == 0){
            $.loading();
            $.ajax({
                type:"get",
                url:purchase.Root+"user/smsCodeNoSign.json?mobile="+phone,
                success:function(data){
                    $.loaded();
                    var t=60;
                    checkTimer=setInterval(function(){
                        if(t==0){
                            clearInterval(checkTimer);
                            checkTimer = 0;
                            $("#getChecked").html("获取验证码");
                        }else{
                            t--;
                            $("#getChecked").html(t+"s");
                        }
                    },1000);
                }
            });
        }
    });
    $("#submit").click(function(){
        MtaH5.clickStat('2',{'5':'true'});
        var getPhone = $("#phone").val();//获取受邀人手机号
        if(getPhone == ""||getPhone == null){
            $("#phone").focus();
            showMsg("请输入手机号");
            return false;
        }
        if(!checked){
            showMsg("请勾选服务协议");
            return false;
        }
        if(!purchase.checkTel(getPhone)){
            $("#phone").focus();
            showMsg("请输入正确手机号");
            return false;
        }
        var smsCode = $('#checked').val();
        if(smsCode == ""||smsCode == null){
            $("#checked").focus();
            showMsg("请输入验证码");
            return false;
        }
        $.loading();
        $.ajax({
            type:"get",
            url:purchase.Root+"user/inviteFrendNew.json?phone="+getPhone+"&invationCode="+ydjrcode+"&smsCode="+smsCode,
            success:function(data){
                $.loaded();
                if(data.code != '000'){
                    showMsg(data.result||'系统错误');
                }else{
                    MtaH5.clickStat('invit_friend',{'setin':'true'})
                    window.location ="invit_down.html?channel=invitation&ydjrphone="+phone1+"&ydjraddtime="+time;
                }

            }
        });
    });
    function showMsg(txt){
        $.popupTxt("form","<span class='show'>"+txt+"</span>",2000);
    }
})
