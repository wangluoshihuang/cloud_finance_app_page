var app = new Vue({
    el: '#activity',
    data: {
        apple: "*声明：此活动与苹果公司无关",
        token: 0,
        isLogin: false,
        res: {},
        popShow: false,
        c: {},
        num: 0,
        ydjraddtime:'',
        ydjrcode:'',
        ydjrphone:'',
        inviteShareQC:''
    },
    created: function () {
        $("#activity").show();
        var _this = this;
        if (window.mobileUtil.isAndroid) {
            _this.apple = "";
        }
        _this.c = new Cloud();
        _this.token = _this.c.getUrlParam('data');
        if (_this.token != undefined && _this.token != "") {
            _this.isLogin = true;
        }
        _this.init();
        _this.bindBtn = window.setTimeout(function () {
            _this.bindBtn = true;
        }, 1000);
    },
    methods: {
        //页面初始化
        init: function () {
            var _this = this;
            $.loading();
            var Da = {
                token:_this.token,
                qrCodeType:'1'
            }
            $.post(_this.c.Root+"user/inviteFriendInfoH5.json",Da, function(e){
                console.log(e)
                if(e.code == '0'){
                    $.loaded();
                    e.result.userCommission = (e.result.userCommission*1.00 + e.result.userCommissioned*1.00).toFixed(2);
                    _this.res = e.result;
                    _this.ydjraddtime = _this.res.ydjraddtime;
                    _this.ydjrcode = _this.res.ydjrcode;
                    _this.ydjrphone = _this.res.ydjrphone;
                    _this.inviteShareQC = _this.res.inviteShareQC;
                    // for (var key in e.result) {
                    //     if (e.result.hasOwnProperty(key)) {
                    //         var element = e.result[key];
                    //         _this.res.result.push(element);
                    //     }
                    // }
                    _this.$nextTick(function() {
                    })
                }
            });
        },
        //跳转到客户端
        goOn: function (key, opt) {
            var _this = this;
            if (_this.bindBtn != true) {
                return false;
            }
            if (_this.isLogin == false) {
                _this.c.goLogin();
                return false;
            }
            _this.c.on(key, opt);
            return false;
        },
        //分享好友-二维码
        goShareCode: function () {
            if (this.bindBtn != true) {
                return false;
            }
            var _this = this;
            var data = {
                // url: _this.c.shareRoot + "com/page/invite_friend/invit_share.html",
                imageUrl: _this.inviteShareQC
                // title: "您的好友送您666元理财红包，点击领取",
                // content: '签约存管，国企控股，安全可靠。一起来云端赚钱吧！'
            }
            if (_this.isLogin) {
                _this.stat('sharecode');
                _this.c.goShareCode(data);
                _this.chart("shareChart",_this.c.Root+"newInvitation/records.json?data="+_this.token+"&url="+encodeURIComponent(data.url));
            } else {
                _this.c.goLogin();
                return false;
            }
        },
        //分享好友-链接
        goShare: function () {
            if (this.bindBtn != true) {
                return false;
            }
            var _this = this;
            console.log(_this.ydjraddtime)
            var data1 = {
                url: _this.c.shareRoot + "com/page/invite_friend/invit_share.html?ydjraddtime="+_this.ydjraddtime+'&ydjrcode='+_this.ydjrcode+'&ydjrphone='+_this.ydjrphone,
                title: "您的好友送您666元理财红包，点击领取",
                content: '签约存管，国企控股，安全可靠。一起来云端赚钱吧！'
            }
            if (_this.isLogin) {
                _this.stat('share');
                _this.c.goShare(data1);
                _this.chart("shareChart",_this.c.Root+"newInvitation/records.json?data="+_this.token+"&url="+encodeURIComponent(data1.url));
            } else {
                _this.c.goLogin();
                return false;
            }
        },
        chart: function (id, url) {
            var _this = this;
            if (document.getElementById(id)) {
                document.getElementById(id).src = url;
            } else {
                var iframe = document.createElement('iframe');
                iframe.id = "shareChart";
                iframe.name = "shareChart";
                iframe.src = url;
                iframe.style.width = "0px";
                iframe.style.height = "0px";
                iframe.style.boder = "0px;";
                iframe.style.position = "position";
                document.body.appendChild(iframe);
            }
        },
        goRule: function () {
            var _this = this
            _this.popShow = true;
            _this.stat('rule');
        },
        closeRule: function () {
            var _this = this
            _this.popShow = false;
        },
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('invit_friend',d);
        }
    }
});
