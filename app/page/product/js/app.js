
 var app = new Vue({
    el:'#product',
    data:{
        list:[],
        c:{}
    },
    created: function () {
        var _this = this;
        _this.c=new Cloud();
        _this.init(); 
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $(".loadingPage").show();
            $.ajax({
            url: _this.c.Root+"common/projects.json",
            type: "get",
            dataType:'json',
            success:function(data){
                $(".loadingPage").hide();
                _this.list=data.ProjectList;

            },
            error:function(er){
                $(".loadingPage").hide();
               
            }
            });
        }
    }
 });
