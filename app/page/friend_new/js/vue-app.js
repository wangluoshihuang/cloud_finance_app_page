var app = new Vue({
    el:'#friend_new',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:{},
        page:{isRule:false,isBegin:false},
        user:{},
        friends:[],
        isMore:false,
        c:{}
    },
    created: function () {
        $("#friend_new").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init();
        _this.getFriends();   
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            _this.getCode();
            $.loading();
            $.ajax({
                url: _this.c.Root+"newInvitation/index.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                   _this.result = data; 
                   if(data.activity_status == 1){
                       _this.page.isBegin = false;
                   }else{
                      _this.page.isBegin = true;
                   }    
                },
                error:function(er){
                     $.loaded();
                }
            });
        },
       getFriends:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: _this.c.Root+"newInvitation/friends.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                    for(var i =0;i<data.friendsList.length;i++){
                        if(i<3){
                             _this.friends.push(data.friendsList[i]);
                        }else{
                            _this.isMore = true;
                            break;
                        }
                    }    
                },
                error:function(er){
                     $.loaded();
                }
            });
       }, 
       openRule:function(){
             this.page.isRule = true;
             $("#activity").css("position","fixed");
             $("#activity").css("top",0);
             $("#activity").css("overflow","hidden");
        },
        closeRule:function(){
            this.page.isRule = false;
            $("#activity").css("position","static");
            $("#activity").css("height","auto");
            $("#activity").css("overflow","auto");
        },
        goFriends:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            window.location.href = _this.c.pageRoot+"com/page/friend_new/friends.html?data=" + _this.token ;
        },
        getCode:function(){
            var _this = this;
                $.ajax({
                url: _this.c.Root+"invite/getAct.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                   if(data.code == 1){
                    _this.user=data;
                   }
                },
                error:function(er){
                   
                }
            });
        },
        //分享好友
        goShare:function(){
             if(this.bindBtn != true){
                return false;
            }
            var _this = this;

            var data = {
                url:_this.c.shareRoot+"build/html/invitation/index.html?type=1&ydjrphone="+
                _this.user.username+"&ydjrcode="+_this.user.invitation_code,
                title:"云端金融 16%往期平均收益率",
                content:"签约存管，国资背景，安全可靠。新人领666红包！"
            }
            if(_this.isLogin) {
                _this.c.goShare(data);
                _this.chart("shareChart",_this.c.Root+"newInvitation/records.json?data="+_this.token+"&url="+encodeURIComponent(data.url));
            }else{
               
                _this.goOn('goLogin');
                return false;
            }
            
        },
        chart:function(id,url){
            var _this = this;
            if(document.getElementById(id)){
                document.getElementById(id).src=url;

            }else{
                var iframe = document.createElement('iframe');
                iframe.id="shareChart";
                iframe.name="shareChart";
                iframe.src=url;
                iframe.style.width="0px";
                iframe.style.height="0px";
                iframe.style.boder="0px;";
                iframe.style.position="position";
                document.body.appendChild(iframe);
            }
            setTimeout(function(){
                _this.pop = {show:false,title:'',key:''};
                _this.prize={type:'',value:''};
                 $(".page-pop").hide();
                _this.init();
            },1500)
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        }
    }
 });
