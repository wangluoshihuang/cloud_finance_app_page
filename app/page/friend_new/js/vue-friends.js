var app = new Vue({
    el:'#friends',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:{},
        page:{isRule:false,isBegin:false},
        user:{},
        friends:[],
        isMore:false,
        c:{}
    },
    created: function () {
        $("#friends").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: _this.c.Root+"newInvitation/friends.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                    _this.friends=data.friendsList; 
                },
                error:function(er){
                     $.loaded();
                }
            });
        },
        goIndex:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            window.location.href = _this.c.pageRoot+"com/page/friend_new/index.html?data=" + _this.token ;
        }
    }
 });
