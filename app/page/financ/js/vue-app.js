      
 var app = new Vue({
    el:'#financ',
    data:{
        timer:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        isRoll:false,
        isQuan:0,
        result:0,
        bindBtn:0,
        pop:{show:false,title:'',key:''},
        page:{isRule:false,isBegin:false,isPreheating:false},
        poplayer:{show:false,text:''},
        roleList:[],
        user:{},
        prize:{type:'',value:''},
        startBtn:true,
        c:{}
    },
    created: function () {
        $("#financ").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init();   
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            _this.getCode();
            $.loading();
            $.ajax({
                url: _this.c.Root+"lottery/index.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded()
                     _this.result = data;
                     switch (data.activity_status) {
                        case 1:
                         _this.page.isPreheating = true;
                         break;
                        case 2:
                         _this.page.isBegin = true; 
                         
                         _this.roleList = data.coupon_list_all;
                         _this.roleList.push(data.coupon_list);
                         if(data.is_lottery != 1){
                            _this.poplayer.show = true;
                           _this.poplayer.text = '<div class="d1">剩余'+data.equivalent_numer+'抽奖机会</div>';
                           _this.poplayer.text += '<div class="d2">'+data.msg+'<div class="d1">';

                         }
                         break;
                        case 3:
                          _this.page.isBegin = true;
                          _this.page.isEnd = true;
                          _this.poplayer.show = true;
                          _this.poplayer.text = '<div class="title">活动已结束！</div>';
                         break;
                     }  
                    if(data.status == 101){
                        _this.c.goLogin();
                    }else if(data.status != 200){
                        var opt={
                            title:data.msg,
                        };
                        $.openPop(opt);
                    }
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        },
        getCode:function(){
            var _this = this;
                $.ajax({
                url: _this.c.Root+"invite/getAct.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                   if(data.code == 1){
                    _this.user=data;
                   }
                },
                error:function(er){
                   
                }
            });
        },
        //埋点
        stat:function(id){
            var d={};
            d[id] = 'true';
            MtaH5.clickStat('aact',d);
        },
        //跳转到我的券包
        goOn:function(key){
             var _this = this;
            _this.pop = {show:false,title:'',key:''};
            _this.prize={type:'',value:''};
             $(".page-pop").hide();
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key);
            switch(key){
                case "goLogin":
                    _this.stat('gologin');
                    break;
                case "goProductList":
                     _this.stat('goproductlist');
                    break;
                case "goBuyPackage":
                    _this.stat('gobuypackage'); 
                    break;  
                case "goWalletRecord":
                     _this.stat('gowalletrecord'); 
                    break;      
                }
           return false;
        },
        openPage:function(key,type){
            var _this =this;
              _this.isRoll = false;
                _this.isQuan = 0;
                for(var k in _this.page){
                if(k == key){
                    _this.page[k] = true;
                }else{
                        if(type){
                             _this.page[k] = false;
                        }
                    
                    }
                }
            if(_this.page.isRule){
                 _this.stat('rule');
            }    
 
        },
        closePop:function(){
            var _this = this;
            _this.pop = {show:false,title:'',key:''};
            _this.prize={type:'',value:''};
            $(".page-pop").hide();
            _this.init();
        },
        //分享好友
        goShare:function(){
            this.stat('share');
             if(this.bindBtn != true){
                return false;
            }
            var _this = this;

            var data = {
                url:_this.c.pageRoot+"build/html/invitation/index.html?type=1&ydjrphone="+
                _this.user.username+"&ydjrcode="+_this.user.invitation_code,
                title:"云端金融 16%高收益",
                content:"我是第"+_this.result.share_times+"位恭喜云端金融获A轮融资用户，邀请你来一起狂欢，拿666元现金红包"
            }
            if(_this.isLogin) {
                _this.c.goShare(data);
                _this.chart("shareChart",_this.c.Root+"lottery/addChance.json?data="+_this.token+"&url="+encodeURIComponent(data.url));
            }else{
               
                _this.goOn('goLogin');
                return false;
            }
            
        },
        chart:function(id,url){
            var _this = this;
            if(document.getElementById(id)){
                document.getElementById(id).src=url;

            }else{
                var iframe = document.createElement('iframe');
                iframe.id="shareChart";
                iframe.name="shareChart";
                iframe.src=url;
                iframe.style.width="0px";
                iframe.style.height="0px";
                iframe.style.boder="0px;";
                iframe.style.position="position";
                document.body.appendChild(iframe);
            }
            setTimeout(function(){
                _this.pop = {show:false,title:'',key:''};
                _this.prize={type:'',value:''};
                 $(".page-pop").hide();
                _this.init();
            },1500)
        },
        //开始抽奖
        beginRoll:function(){
           
            var _this =this;
             _this.stat('start');
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            if(_this.startBtn){
                 _this.startBtn = false;
            }else{
               return false;
            }
            $.loading();
            $.ajax({
                url: _this.c.Root+"lottery/lottery.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                    _this.init();
                   if(data.status == 200){
                        _this.isRoll = true;
                        _this.isQuan = 1;
                        _this.prize = data;
                       
                        setTimeout(function(){
                            _this.isRoll = false;
                            _this.isQuan = 2;
                            var msg = "",key = "";

                             switch (data.type) {
                                case 1:
                                 msg = "恭喜您"+data.value+"%加息券到手";
                                 key = "goBuyPackage";
                                 break;
                                case 2:
                                 msg = "恭喜您"+data.value+"元返现券到手";
                                 key = "goBuyPackage";
                                 break;
                                case 3:
                                 msg = "恭喜您"+data.value+"现金到手";
                                 key = "goWalletRecord";
                                 break;  
                             }
                           _this.pop = {show:true,title:msg,key:key};

                            $(".page-pop").show();
                            _this.startBtn = true;
                        },1500);
                    }else if(data.status == '101'){
                        _this.goOn('goLogin');
                         _this.startBtn = true;
                        return false;
                    }else{
                        var opt={
                            title:data.msg,
                        };
                        $.openPop(opt);
                         _this.startBtn = true;
                    }
                },
                error:function(er){
                    $.loaded();
                   
                }
            });

            

        },
        //前往我的奖品
        goPrizeList:function(){
            this.stat('prizelist');
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            window.location.href = _this.c.pageRoot+"com/page/financ/myprizelist.html?data=" + _this.token + 
            "&username=" + _this.user.username + "&invitation_code=" + _this.user.invitation_code + "&num="+_this.result.share_times;
        }
    }
 });
