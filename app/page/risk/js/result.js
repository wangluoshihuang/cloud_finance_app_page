var app = new Vue({
    el:'#result',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        res:{
            name:'保守型',
            desc:'风险提示:您的投资风险测评属于保守型，保住本金是您投资第一目标，适合极低风险产品'
        },
        c:{}
    },
    created: function () {
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            _this.$nextTick(function() {
                var Da1 = {
                    token:_this.token
                }
                $.post(_this.c.Root+"user/getRiskTestResult.json",Da1, function(e){
                    if(e.code == 0){
                        _this.res.name = e.result.type;
                        _this.res.desc = e.result.scheme;
                    }
                });
                // $.post("https://api.test.yd-jr.cn/api/user/getRiskTestResult",Da1, function(e){
                //     if(e.code == 0){
                //         _this.res.name = e.result.type;
                //         _this.res.desc = e.result.scheme;
                //     }
                // });
                $("#result").show();
            })
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        retest:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            window.location.href = 'index.html?data='+_this.c.getUrlParam('data');
        },
        invest:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            // alert('去理财');
            _this.goOn('goProductList')
            // _this.c.toFinish();
        }
    }
 });
