var app = new Vue({
    el:'#risk',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        count:1,
        width:'',
        answer:[],
        ans:[],
        res:{result:[]},
        c:{},
        preNum:10
    },
    created: function () {
        
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init(); 
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
           
            $.loading();
            var Da = {
                token:_this.token
            }
            $.post(_this.c.Root+"user/getRiskTestSubject.json",Da, function(e){
                console.log(e)
                if(e.code == 0){
                    $.loaded();
                    $("#risk").show();
                    for (var key in e.result) {
                        if (e.result.hasOwnProperty(key)) {
                            var element = e.result[key];
                            _this.res.result.push(element);
                        }
                    }
                    _this.$nextTick(function() {
                        // console.log(_this.res.result.length)
                        _this.width = $('.pro').width()/_this.res.result.length;
                        $('.blue').width(_this.width)
                        // console.log(_this.width,$('.pro').width())
                    })
                }
            });
            // $.post("https://api.test.yd-jr.cn/api/user/getRiskTestSubject",Da, function(e){
            //     console.log(e)
            //     if(e.code == 0){
            //         $.loaded();
            //         $("#risk").show();
            //         for (var key in e.result) {
            //             if (e.result.hasOwnProperty(key)) {
            //                 var element = e.result[key];
            //                 _this.res.result.push(element);
            //             }
            //         }
            //         _this.$nextTick(function() {
            //             // console.log(_this.res.result.length)
            //             _this.width = $('.pro').width()/_this.res.result.length;
            //             $('.blue').width(_this.width)
            //             // console.log(_this.width,$('.pro').width())
            //         })
            //     }
            // });
            
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        select:function(d,index){
            var _this = this;
            d.orderNum = parseInt(d.orderNum);
            var wid = _this.width*(d.orderNum+1);
            var length1 = $('.pic').eq(d.orderNum-1).find('.selected').length;
            _this.$nextTick(function() {
                $('.pic').eq(d.orderNum-1).find('li').removeClass('selected');
                $('.pic').eq(d.orderNum-1).find('li').eq(index).addClass('selected');
                $('.pic').eq(d.orderNum-1).find('img').attr('src','img/img_yuan.png');
                $('.pic').eq(d.orderNum-1).find('img').eq(index).attr('src','img/img_dian.png');
                if(length1== 0){
                    var obj = new Object();
                    obj.subjectId = d.subjectId;
                    obj.optionId = d.riskTestOption[index].optionId;
                    obj.score = d.riskTestOption[index].score;
                    // list.push(temp);
                    _this.ans.push(obj)
                    // _this.answer.shift(d.orderNum-1);
                }else{
                    var obj1 = new Object();
                    obj1.subjectId = d.subjectId;
                    obj1.optionId = d.riskTestOption[index].optionId;
                    obj1.score = d.riskTestOption[index].score;
                    _this.ans[d.orderNum-1]=obj1;
                    console.log(_this.ans,_this.answer);
                    if(_this.answer.length == 9){
                        
                    }else if(_this.answer.length != 9){
                        _this.answer.shift(d.orderNum-1);
                    }
                }
                console.log(_this.ans,_this.answer);
                if(d.orderNum == _this.res.result.length){
                    return false;
                }
                setTimeout(function(){
                    _this.count = d.orderNum+1;
                    $('.blue').width(_this.width*(_this.count));
                    if(_this.answer.length == 9){
                        return false;
                    }
                    _this.answer.push(d.orderNum);
                },100)
            })
        },
        prev:function(d){
            d.orderNum = parseInt(d.orderNum);
            // console.log(d.orderNum)
            if(d.orderNum==10){
                this.preNum=9;
            }
            this.count = d.orderNum-1;
            $('.blue').width(this.width*(this.count));
        },
        nex:function(d){
            d.orderNum = parseInt(d.orderNum);
            this.count = d.orderNum+1;
            $('.blue').width(this.width*(this.count));
        },
        submit:function(d){
            d.orderNum = parseInt(d.orderNum);
            var _this = this;
            var len = $('.pic').eq(d.orderNum-1).find('.selected').length;
            _this.$nextTick(function() {
                if(len == 0){
                    $(".popCon").show();
                }else{
                    console.log(_this.ans)
                    var Da1 = {
                        token:_this.token,
                        riskResult:JSON.stringify(_this.ans)
                    }
                    $.post(_this.c.Root+"user/submitRiskTestResult.json",Da1, function(e){
                        console.log(e)
                        if(e.code == 0){
                        _this.c.exitPop();
                           window.location.href = 'result.html?data='+_this.token;
                        }
                    });
                    // $.post("https://api.test.yd-jr.cn/api/user/submitRiskTestResult",Da1, function(e){
                    //     console.log(e)
                    //     if(e.code == 0){
                    //         _this.c.exitPop();
                    //        window.location.href = 'result.html?data='+_this.token;
                    //     }
                    // });
                   
                }
            })
        },
        popClose:function(){
            $(".pop").hide();
        },
        exit:function(){
            var _this = this;
            _this.c.unFinish();
        }
    }
 });
