var app = new Vue({
    el: '#experience',
    data:{
        incomeList:{
            one:{
                tiltle:"募集失败退款",
                time:"2018-04-09 14:40",
                other:"+5000.00"
            },
            two:{
                tiltle:"体验金2",
                time:"2016/06/08 18:31",
                other:"30.12"
            },
            three:{
                tiltle:"体验金3",
                time:"2016/06/08 18:31",
                other:"30.12"
            },
            dshf2: {
                tiltle: "体验金1",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdf3: {
                tiltle: "体验金2",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdfs4f: {
                tiltle: "体验金3",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            dshf: {
                tiltle: "体验金1",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdf: {
                tiltle: "体验金2",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdfsf: {
                tiltle: "体验金3",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdfsf34: {
                tiltle: "体验金3",
                time: "2016/06/08 18:31",
                other: "30.12"
            },
            sdfserf: {
                tiltle: "体验金3",
                time: "2016/06/08 18:31",
                other: "30.12"
            }
        },
        exportList:{
            one:{
                tiltle:"投资",
                time:"2016/06/08 18:31",
                money:"-5000.00",
                state:"已到账"
            },
            two:{
                tiltle:"投资",
                time:"2016/06/08 18:31",
                money:"-5000.00",
                state:"已到账"
            },
            three:{
                tiltle:"体验金6",
                time:"2016/06/08 18:31",
                money:"-5000.00",
                state:"已到账"
            }
        },
        experiencebl:true,
        experienceactive:"active",
        rewardbl:false,
        rewardactive:null,
        blank:false

    },
    watch: {
        experiencebl:function(){
            if(this.experiencebl){
                this.rewardbl = false;
            }else{
                this.rewardbl = true;
            }
        },
        rewardbl:function () {
            if(this.rewardbl){
                this.experiencebl = false;
            }else{
                this.experiencebl = true;
            }
        }
    },
    methods: {
        showexperience: function () {
            // if(有数据){
            //     this.experiencebl  = true;
            // }else{
            //         this.blank = true;
            // }

            this.experienceactive = "active";
            // this.experiencebl  = true;
            this.rewardactive = null;
        },
        showreward:function(){
            // if(有数据){
            //     this.rewardbl = true;
            // }else{
            //   this.blank = true;
            // }

            this.rewardactive = "active";
            // this.rewardbl = true;
            this.experienceactive = null;
        }
    }
});

$(function(){
    // 页数
    var page = 0;
    // 每页展示10个
    var size = 10;

    // dropload
    $('#upList').dropload({
        scrollArea : window,
        domUp : {
            domClass   : 'dropload-up',
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            domUpdate  : '<div class="dropload-update" style="font-size: 30px;">↑释放更新</div>',
            domLoad    : '<div class="dropload-load"><img style="width: 50px;" src="../../lab/img/07.gif" alt=""></div>'
        },
        loadUpFn : function(me){
            $.ajax({
                type: 'GET',
                url: '',
                dataType: 'json',
                success: function(data){
                    var result = '';
                    for(var i = 0; i < data.lists.length; i++){
                        result +=   '<a class="item opacity" href="'+data.lists[i].link+'">'
                            +'<img src="'+data.lists[i].pic+'" alt="">'
                            +'<h3>'+data.lists[i].title+'</h3>'
                            +'<span class="date">'+data.lists[i].date+'</span>'
                            +'</a>';
                    }
                    // 为了测试，延迟1秒加载
                    setTimeout(function(){
                        $('.lists').html(result);
                        // 每次数据加载完，必须重置
                        me.resetload();
                        // 重置页数，重新获取loadDownFn的数据
                        page = 0;
                        // 解锁loadDownFn里锁定的情况
                        me.unlock();
                        me.noData(false);
                    },1000);
                },
                error: function(xhr, type){
                    // alert('Ajax error!');
                    // 即使加载出错，也得重置
                    me.resetload();
                }
            });
        },
        threshold : 50
    });
});