      
 var app = new Vue({
    el:'#teacher',
    data:{
        timer:0,
        timer1:0,
        timer2:0,
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:0,
        bindBtn:0,
        textBtn:"",
        pop:{show:false,title:'',key:''},
        page:{isRule:false,isActivity:true,isPop:false,isPrize:false},
        roleList:[],
        msg:"",
        btn1:false,
        btn2:false,
        btn3:false,
        btn4:false,

        prize:{type:'',value:''},
        startBtn:true,
        prize:{1:true,2:true,3:true,4:true,5:true,6:true,7:true,8:true},
        timers:{hour:'00',minute:'00',second:'00'},
        prizeItem:4,
        prizeList:[],
        prizeListStatus:0,
        project_ordinary:{id:0},
        c:{}
    },
    created: function () {
        $("#teacher").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
        _this.isLogin = true;
        }
        _this.init();   
        _this.bindBtn=window.setTimeout(function(){
           _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: _this.c.Root+"teacherDay/index.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded()
                     _this.result = data;
                     switch (data.activity_status) {
                        case 1:
                          
                         break;
                        case 2:
                         if(_this.result.lottery_status == 2 && _this.result.countdown >0){
                           
                            _this.timerCorrect(_this.result.countdown);
                         } 
                         //定时刷新剩余抽奖次数
                         _this.getPrizeNum();
                         break;
                        case 3:
                          window.clearInterval(_this.timer1);

                         break;
                     }  
                },
                error:function(er){
                     $.loaded();
                   
                }
            });

        },
        openRule:function(){
             this.page.isRule = true;
             $("#activity").css("position","fixed");
             $("#activity").css("top",0);
             $("#activity").css("height",1749);
             $("#activity").css("overflow","hidden");
        },
        closeRule:function(){
            this.page.isRule = false;
            $("#activity").css("position","static");
            $("#activity").css("height","auto");
            $("#activity").css("overflow","auto");
        },
        openPrize:function(){
             this.page.isPrize = true;
             $("#activity").css("position","fixed");
             $("#activity").css("top",0);
             $("#activity").css("height",1749);
             $("#activity").css("overflow","hidden");
        },
        closePrize:function(){
            this.page.isPrize = false;
            $("#activity").css("position","static");
            $("#activity").css("height","auto");
            $("#activity").css("overflow","auto");
        },
        openPop:function(){this.page.isPop =true},
        closePop:function(){
            var _this=this;
            _this.page.isPop =false;
            if(_this.result&&_this.result.activity_status != 3){
                _this.init();
            }
             
            
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.c.on(key,opt);
           return false;
        },
        //开始抽奖
        beginRoll:function(){
            
            var _this =this; 
            
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.result&&_this.result.activity_status == 3){
                _this.msg="活动已结束,更多精彩敬请期待";
                _this.btn1=false;_this.btn2=false;_this.btn3=true;_this.btn4=false;
                _this.openPop();
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            if(_this.startBtn){
                 _this.startBtn = false;
            }else{
               return false;
            }
            $.loading();
            $.ajax({
                url: _this.c.Root+"teacherDay/lottery.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                    _this.msg=data.msg;
                    _this.getPrizeNum();
                   if(data.status == 200){
                        _this.prize={1:false,2:true,3:true,4:true,5:true,6:true,7:true,8:true};
                        var num=23+data.id;
                        var num2=1;
                        run();
                        function run(){
                            window.setTimeout(function(){

                                var key=num2;
                                _this.prize[key+""]=false;
                                _this.prize[(key-1)+""]=true;
                                if(num2 == 8){
                                    num2 =1;
                                }else{
                                    num2++; 
                                }
                                if(num2 == 1){
                                    _this.prize['8']=true;
                                }
                                if(num ==0){
                                    _this.startBtn = true;
                                    //输出结果
                                    if(data.type == 1 ||data.type == 2){
                                        _this.btn1=true;_this.btn2=true;_this.btn3=false;_this.btn4=false;
                                    }else if(data.type ==5){
                                         _this.project_ordinary={id:data.project_id};
                                        _this.btn1=true;_this.btn2=false;_this.btn3=false;_this.btn4=true;
                                    }
                                    else{
                                        _this.btn1=false;_this.btn2=false;_this.btn3=true;_this.btn4=false;
                                    }
                                    _this.init();
                                    _this.openPop();
                                }else{
                                   num--; 
                                   run();
                                }
                            
                               
                            },100);
                        }

                    }else if(data.status == '101'){
                        _this.goOn('goLogin');
                         _this.startBtn = true;
                        return false;
                    }else{
                         _this.startBtn = true;
                         _this.msg=data.info;
                         _this.btn1=false;_this.btn2=false;_this.btn3=true;_this.btn4=false;
                         _this.openPop();
                    }
                },
                error:function(er){
                    $.loaded();
                   
                }
            });

            

        },
        //剩余抽奖次数
        getPrizeNum:function(){
            var _this = this;
             run();
            window.clearInterval(_this.timer1);

           _this.timer1=window.setInterval(function(){run();},10000);
            function run(){
                $.ajax({
                    url: _this.c.Root+"teacherDay/getChance.json?data="+_this.token,
                    type: "get",
                    dataType:'json',
                    success:function(data){
                        _this.result['changce']=data.changce;
                        _this.result['partake']=data.partake;

                    },
                    error:function(er){

                       
                    }
                });
            }
        },
        goProject:function(){
            var _this = this;

            if(_this.project_ordinary.id && _this.project_ordinary.id!=0){

                 _this.goOn('goPro',_this.project_ordinary);
             }else{

                 _this.goOn('goProductList');
             }
           
        },
        //我的奖品
        getPrizeList:function(){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                 _this.c.goLogin();
                 return false;
            }
            _this.prizeList=[];
            _this.prizeListStatus=0;
            _this.openPrize();
            $.loading();
            $.ajax({
                url: _this.c.Root+"teacherDay/lotteryList.json?data="+_this.token,
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                     if(data.lotteryList&&data.lotteryList.length>0){
                      _this.prizeList = data.lotteryList; 
                      _this.prizeListStatus=1;
                     }else{
                      _this.prizeListStatus=2;
                     }     
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        },
        timerCorrect:function(num){
            var _this = this;
             _this.startTimer(num);
            window.clearInterval(_this.timer2);
            _this.timer2=window.setInterval(function(){run();},60000);
            function run(){
                $.ajax({
                    url: _this.c.Root+"teacherDay/getCountDown.json?data="+_this.token,
                    type: "get",
                    dataType:'json',
                    success:function(data){
                      _this.startTimer(data);

                    },
                    error:function(er){
                        
                    }
                });
            }
        },
        //倒计时
        startTimer:function(num){
            _this = this;
             setTimer(num);
            window.clearInterval(_this.timer);
            _this.timer=window.setInterval(function(){
                num--;
                setTimer(num);

            }, 1000);
            function setTimer(intDiff){
                if(intDiff < 1){
                    window.clearInterval(_this.timer);
                    _this.timer = 0;
                    _this.init();
                    return false;
                }
                var day=0,
                    hour=0,
                    minute=0,
                    second=0;//时间默认值
                if(intDiff > 0){
                    day = Math.floor(intDiff / (60 * 60 * 24));
                    hour = Math.floor(intDiff / (60 * 60)) ;
                    minute = Math.floor(intDiff / 60)  - (hour * 60);
                    second = Math.floor(intDiff) - (hour * 60 * 60) - (minute * 60);
                }
                if (hour <= 9) hour = '0' + hour;
                if (minute <= 9) minute = '0' + minute;
                if (second <= 9) second = '0' + second;
               // $('#day_show').html(day+"天");
                _this.timers.hour=hour;
                _this.timers.minute=minute;
                _this.timers.second=second;
            }
        }
    }
 });
