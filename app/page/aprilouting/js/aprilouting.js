$(function () {
    /**/
    function getServerDate(){
        return new Date($.ajax({
            async: false,
            url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/info/get'//测试
        }).getResponseHeader("Date"));
    };
    if(window.mobileUtil.isAndroid){
        $('.apple').html('');
    }
    // var nowTime = Date.parse(new Date());//本地时间 方便测试
    var nowTime = Date.parse(getServerDate());//服务器时间
    var endTime = Date.parse('2018/04/16 23:59:59');
    var c = new Cloud();
    var toKen= c.getUrlParam('data');
    var topicsecret = '09FE7B8AF9D7EEF7217C2D43EF11DE5C';
    window.alert = function(name){
        var iframe = document.createElement("IFRAME");
        iframe.style.display="none";
        iframe.setAttribute("src", 'data:text/plain,');
        document.documentElement.appendChild(iframe);
        window.frames[0].window.alert(name);
        iframe.parentNode.removeChild(iframe);
    };
    var numSp = function (num) {
        var newNum;
        var s = num.toString().split('.')[1];
        if(s == undefined){
            newNum = num.toLocaleString('en-US')+'.'+'00';
        }else{
            newNum = num.toLocaleString('en-US');
        }
        return newNum;
    }
    var isLogin = function () {
        $.ajax({
            url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/springout/info/get',
            data:{
                token:toKen,
                topicsecret:topicsecret
            },
            type:'POST',
            dataType:'json',
            success:function (data) {
                if(data.code){
                    /*接口报错*/
                    if(data.code === '10002'){
                        /*用户未登录*/
                        $('#rotate img').attr('src','img/xq-btn-5.2.png');
                        $('.surplus').hide();
                        $('#myreward').on('click',function () {
                            c.goLogin();
                        });
                        $('.address').on('click',function () {
                            c.goLogin();
                        });
                        $('#rotate').addClass('nologin');
                    }
                }else{
                    /*请求成功*/
                    $('#sub90').text(numSp(data.invest90subjet));
                    $('#sub360').text(numSp(data.invest360subjet));
                    $('#invistAmount').text(data.invitation);
                    getRollTimes();
                    $('.address').on('click',function () {
                        window.location.href = c.pageRoot+"com/page/aprilouting/address.html?data="+toKen
                    });
                    $('#myreward').on('click',function () {
                        window.location.href = c.pageRoot+"com/page/aprilouting/myreward.html?data="+toKen
                    })
                }

            },
            error:function () {
            }
        })
    }
    var getRollTimes = function () {
        $.ajax({
            url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/lottery/times/get',
            type:'POST',
            dataType:'json',
            data:{
                token:toKen,
                topicsecret:topicsecret
            },
            success:function (data) {
                if(!data.code){
                    $('.surplus span').text(data.times);
                    if(data.used<10){
                        /*抽奖次数小于10次*/
                        if(data.times == 0){
                            $('#rotate').addClass('notimes');
                        }
                    }else{
                        /*抽奖次数大于10次*/
                        $('#rotate').addClass('ten');
                        $('#rotate img').attr('src','img/xq-btn-5.4.png');
                        $('.surplus').addClass('hasten');
                    }
                }
            },
            error:function () {

            }
        });
    }
    var showList = function () {
        $.ajax({
            url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/broadcasting/list/get',
            type:'POST',
            dataType:'json',
            data:{
                topicsecret:topicsecret
            },
            success:function (data) {
                if(!data.code){
                    $.each(data.list,function (index,item) {
                        $('.recordbox ul').append('<li>'+item.prizeContent+'</li>');
                    });
                }
                $('.recordbox div').kxbdMarquee({
                    direction:"up",
                    loop:0,
                    scrollDelay:40,
                    isEqual: true
                });
            },
            error:function () {

            }
        });
    }

    var reward = function () {
        var click = false;
        var n;
        var rewardsign = {
            index: -1,
            count: 0,
            timer:0,
            speed:20,
            times:0,
            cycle:20,
            prize:-1,
            init:function (id) {
                if($('.'+id).find('.rewardsign').length>0){
                    $rewardsign = $('.'+id);
                    $rewardsigns = $rewardsign.find('.rewardsign');
                    this.obj = $rewardsign;
                    this.count = $rewardsigns.length;
                    $rewardsign.find('.reward'+this.index).addClass('active');
                }
            },
            roll:function () {
                   var index = this.index;
                   var count = this.count;
                   var rewardsign = this.obj;
                   $(rewardsign).find('.reward'+index).removeClass('active');
                   index+=1;
                   if(index>count){
                       index=0
                   }
                   $(rewardsign).find('.reward'+index).addClass('active');
                   this.index = index;
                   return false;
            },
            stop:function () {
              this.prize = index;
              return false;
            }
        };
        function roll() {
            rewardsign.times +=1;
            rewardsign.roll();
            if(rewardsign.times>rewardsign.cycle+13&&rewardsign.prize == rewardsign.index){
                $('.reward'+rewardsign.index).removeClass('active');
                $('.reward'+(rewardsign.index+1)).addClass('active');
                clearTimeout(rewardsign.timer);
                rewardsign.prize = 1;
                rewardsign.index = 0;
                click = false;
            }else{
                if(rewardsign.times<rewardsign.cycle){
                    rewardsign.speed-=10;
                }else if(rewardsign.times == rewardsign.cycle){
                    rewardsign.prize = n;
                }else{
                    if(rewardsign.times>rewardsign.cycle+13&&((rewardsign.prize == 0&&rewardsign.index ==7)||rewardsign.prize==rewardsign.index+1)){
                        rewardsign.speed +=110;
                    }else{
                        rewardsign.speed+=20;
                    }
                }
                if(rewardsign.speed<40){
                    rewardsign.speed = 40;
                }
                rewardsign.timer = setTimeout(roll,rewardsign.speed);
            }
            return false;
        }
        rewardsign.init('rotatebox');
        $('#rotate').on('click',function () {
            if($(this).hasClass('nologin')){
                c.goLogin();
                return false;
            }
            if($(this).hasClass('ten')){
                return false;
            }
            if($(this).hasClass('double')){
                return false;
            }
            if($(this).hasClass('notimes')){
                showReault('notimes');
                return false;
            }
            if(click){
                return false;
            }else{
                $.ajax({
                    url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/lottery/luckdraw',
                    type:'POST',
                    typeData:'json',
                    data:{
                        token:toKen,
                        topicsecret:topicsecret
                    },
                    success:function (data) {
                        if(!data.code){
                            $('#rotate').addClass('double');
                            $('.rewardsign').removeClass('active');
                            rewardsign.speed = 80;
                            rewardsign.times = 0;
                            if(data.used<10){
                                $('.surplus span').text(data.times);
                                if(data.times == 0){
                                    $('#rotate').addClass('notimes');
                                }
                            }else{
                                $('#rotate').addClass('ten');
                                $('#rotate img').attr('src','img/xq-btn-5.4.png');
                                $('.surplus').addClass('hasten');
                                $('.surplus span').text(data.times);
                            }
                            if(data.prizeType == 'JX' && data.prizeValue == '2.0%'){
                                n=0;
                                setTimeout(function () {
                                    showReault('congru','2.0%');
                                },6400);
                            }
                            if(data.prizeType == 'FX' && data.prizeValue == '100'){
                                n=1;
                                setTimeout(function () {
                                    showReault('congru','100');
                                },6400);
                            }
                            if(data.prizeType == 'FX' && data.prizeValue == '500'){
                                n=5;
                                setTimeout(function () {
                                    showReault('congru','500');
                                },8100);
                            }
                            if(data.prizeType == 'JD' && data.prizeValue == '5000'){
                                n=2;
                                setTimeout(function () {
                                    showReault('congru','2.0%');
                                },6500);
                            }
                            if(data.prizeType == 'XX'){
                                if(Math.round(Math.random())>5){
                                    n=3;
                                    setTimeout(function () {
                                        showReault('XX');
                                    },4000);
                                }else{
                                    n=9;
                                    setTimeout(function () {
                                        showReault('XX');
                                    },4400);
                                }
                            }
                            if(data.prizeType == 'XJ' && data.prizeValue == '188'){
                                n=4;
                                setTimeout(function () {
                                    showReault('congru','188');
                                },7400);
                            }
                            if(data.prizeType == 'XJ' && data.prizeValue == '50'){
                                n=6;
                                setTimeout(function () {
                                    showReault('congru','50');
                                },8500);
                            }
                            if(data.prizeType == 'XJ' && data.prizeValue == '88'){
                                n=7;
                                setTimeout(function () {
                                    showReault('congru','88');
                                },9200);
                            }
                            if(data.prizeType == 'JD' && data.prizeValue == '1000'){
                                n=8;
                                setTimeout(function () {
                                    showReault('congru','1000');
                                },4200);
                            }
                            if(data.prizeType == 'JD' && data.prizeValue == '500'){
                                n=10;
                                setTimeout(function () {
                                    showReault('congru','JD500');
                                },4500);
                            }
                            if(data.prizeType == 'XJ' && data.prizeValue == '888'){
                                n=11;
                                setTimeout(function () {
                                    showReault('congru','888');
                                },4300);
                            }
                            roll();
                            click = true;
                            return false;
                        }else{
                            if(data.code==='10006'){
                                $('#rotate img').attr('src','img/xq-btn-5.3.png').addClass('ten');
                                $('.surplus').hide();
                                $('.footer a img').attr('src','img/xq-bt-4.png');
                                $('.footer a').addClass('done');
                            }else{
                                alert(data.message);
                            }
                        }
                    },
                    error:function () {

                    }
                });
            }
        });
    }
    var showReault = function (prize, value) {
        $('.dialog').show();
        $('.dialog-reward').show();
        if(prize == 'XX'){
            $('.sorry').show();
        }
        if(prize == 'notimes'){
            $('.getmore').show();
        }
        if(prize == 'congru'){
            $('.congratulation').show();
            $('.congratulation >img').removeClass();
            if(value == '2.0%'){
                $('.congratulation >img').attr('src','img/tan-img-5.png').addClass('jx');
            }
            if(value == '100'){
                $('.congratulation >img').attr('src','img/tan-img-6.png').addClass('jx');
            }
            if(value == '500'){
                $('.congratulation >img').attr('src','img/tan-img-10.png').addClass('jx');
            }
            if(value == '5000'){
                $('.congratulation >img').attr('src','img/tan-img-8.png').addClass('jd');
            }
            if(value == '188'){
                $('.congratulation >img').attr('src','img/tan-img-2.png');
            }
            if(value == '50'){
                $('.congratulation >img').attr('src','img/tan-img-4.png');
            }
            if(value == '88'){
                $('.congratulation >img').attr('src','img/tan-img-1.png');
            }
            if(value == '1000'){
                $('.congratulation >img').attr('src','img/tan-img-9.png').addClass('jd');
            }
            if(value == 'JD500'){
                $('.congratulation >img').attr('src','img/tan-img-7.png').addClass('jd');
            }
            if(value == '888'){
                $('.congratulation >img').attr('src','img/tan-img-3.png');
            }
        }
    }
    if(nowTime<endTime){
        /*活动进行中*/
        isLogin();
        reward();
        $('.footer a').on('click',function () {
            if($(this).hasClass('done')){
                return false;
            }
            c.goProductList();
        });
    }else{
        /*活动结束*/
        $('#rotate img').attr('src','img/xq-btn-5.3.png').addClass('ten');
        $('.surplus').hide();
        $.ajax({
            url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/springout/info/get',
            data:{
                token:toKen,
                topicsecret:topicsecret
            },
            type:'POST',
            dataType:'json',
            success:function (data) {
                if(data.code){
                    /*接口报错*/
                    if(data.code === '10002'){
                        /*用户未登录*/
                        $('#myreward').on('click',function () {
                            c.goLogin();
                        });
                    }
                }else{
                    /*请求成功*/
                    $('#sub90').text(numSp(data.invest90subjet));
                    $('#sub360').text(numSp(data.invest360subjet));
                    $('#invistAmount').text(data.invitation);
                    $('#myreward').on('click',function () {
                        window.location.href = c.pageRoot+"com/page/aprilouting/myreward.html?data="+toKen
                    })
                }

            },
            error:function () {
            }
        });
        $('.footer a img').attr('src','img/xq-bt-4.png');
    }
    showList();
    $('.yes').on('click',function () {
        $('#rotate').removeClass('double');
        $(this).parent('div').hide();
        $('.dialog-reward').hide();
        $('.dialog').hide();
    });
    $('.close').on('click',function () {
        $('#rotate').removeClass('double');
        $(this).siblings('div').hide();
        $('.dialog-reward').hide();
        $('.dialog').hide();
    });
    $('.gotopro').on('click',function () {
        c.goProductList();
    });

    $('.invite').on('click',function () {
        $.ajax({
            url: c.Root+"invite/getAct.json?data="+toKen,
            type: "get",
            dataType:'json',
            success:function(data){
                if(data.code == 1){
                    var dataShare = {
                        url:c.shareRoot+"build/html/invitation/index.html?type=1&ydjrphone="+
                        data.username+"&ydjrcode="+data.invitation_code,
                        title:"云端金融 16%往期平均收益率",
                        content:"签约存管，国资背景，安全可靠。新人领666红包！"
                    };
                    c.goShare(dataShare);
                }
            },
            error:function(er){

            }
        });

    });

});