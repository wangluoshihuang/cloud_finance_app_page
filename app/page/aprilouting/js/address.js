var app = new Vue({
    el:'#address',
    data:{
        apple:"*声明：此活动与苹果公司无关",
        token:0,
        isLogin:false,
        result:{
            provice:"",
            city:"",
            town:""
        },

        c:{}
    },
    created: function () {
        $("#address").show();
        var _this = this;
        if(window.mobileUtil.isAndroid){
            _this.apple ="";
        }
        _this.c=new Cloud();
        _this.token=_this.c.getUrlParam('data');
        if(_this.token != undefined && _this.token != ""){
            _this.isLogin = true;
        }
        _this.init();
        _this.bindBtn=window.setTimeout(function(){
            _this.bindBtn = true;
        },1000);
    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: "https://topic.yd-jr.cn/ydtopic-web/specialtopic/address/info/get",
                type: "get",
                dataType:'json',
                data:{
                  token:_this.token,
                    topicsecret:'09FE7B8AF9D7EEF7217C2D43EF11DE5C'
                },
                success:function(data){
                    $.loaded();
                    if($.isEmptyObject(data)){
                        var p = '选择省';
                        var c= '选择省';
                        var d = '选择省';
                    }else{
                        _this.result = data;
                        var p = (data.provice == '')?'选择省':data.provice;
                        var c= (data.city == '')?'选择市':data.city;
                        var d = (data.town == '')?'选择区':data.town;
                    }
                    $('#distpicker').distpicker({
                        placeholder: false,
                        provice: p,
                        city: c,
                        district: d
                    });

                },
                error:function(er){
                    $.loaded();

                }
            });
        },
        //跳转到客户端
        goOn:function(key,opt){
            var _this = this;
            if(_this.bindBtn != true){
                return false;
            }
            if(_this.isLogin == false) {
                _this.c.goLogin();
                return false;
            }
            _this.c.on(key,opt);
            return false;
        },
        changeAdd:function(e){
            var _this = this;
            var d="";
            setTimeout(function(){
                if($("#district option").length == 0){
                    $("#district").hide();
                    $(".town").hide();
                }else{
                    $("#district").show();
                    $(".town").show();
                    d = $("#district option:checked").text();
                }
                var p = $("#province option:checked").text();
                var c = $("#city option:checked").text();
                _this.result.provice= (p == '选择省') ?'':p;
                _this.result.city=  (c == '选择市') ?'':c;
                _this.result.town=  (d == '选择区') ?'':d;
            },50);
        },
        submit:function(){
            var _this = this;
            _this.result.token=_this.token;
            _this.result.topicsecret='09FE7B8AF9D7EEF7217C2D43EF11DE5C';
            $.loading();
            $.ajax({
                /*url: _this.c.Root+"ydtopic-web/specialtopic/address/info/save",*/
                url: "https://topic.yd-jr.cn/ydtopic-web/specialtopic/address/info/save",
                type: "post",
                data: _this.result,
                dataType:'json',
                success:function(data){
                    $.loaded();
                    if(data.result){
                        window.location.href = _this.c.pageRoot+"com/page/aprilouting/index.html?data=" + _this.token ;
                    }else{
                        var opt={
                            title:data.message
                        };
                        $.openPop(opt);
                    }
                },
                error:function(er){
                    $.loaded();

                }
            });
        }
    }
});
