$(function () {
    var c = new Cloud();
    var token = c.getUrlParam('data');
    $.ajax({
       url:'https://topic.yd-jr.cn/ydtopic-web/specialtopic/broadcasting/mylist/get',
       type:'POST',
       dataType:'json',
       data:{
           token:token,
           topicsecret:'09FE7B8AF9D7EEF7217C2D43EF11DE5C'
       },
       success:function (data) {
            if(data.list.length>0){
                var str='';
                $.each(data.list,function (index) {
                    if(data.list[index].prizeType == 'JX'){
                        str+='<li>恭喜您获得了<span>2.0%加息券</span></li>';
                    }
                    if(data.list[index].prizeType == 'FX'&&data.list[index].prizeValue == '100'){
                        str+='<li>恭喜您获得了<span>100元返现券</span></li>';
                    }
                    if(data.list[index].prizeType == 'FX'&&data.list[index].prizeValue == '500'){
                        str+='<li>恭喜您获得了<span>500元返现券</span></li>';
                    }
                    if(data.list[index].prizeType == 'XJ'&&data.list[index].prizeValue == '50'){
                        str+='<li>恭喜您获得了<span>50元现金红包</span></li>';
                    }
                    if(data.list[index].prizeType == 'XJ'&&data.list[index].prizeValue == '88'){
                        str+='<li>恭喜您获得了<span>88元现金红包</span></li>';
                    }
                    if(data.list[index].prizeType == 'XJ'&&data.list[index].prizeValue == '188'){
                        str+='<li>恭喜您获得了<span>188元现金红包</span></li>';
                    }
                    if(data.list[index].prizeType == 'XJ'&&data.list[index].prizeValue == '888'){
                        str+='<li>恭喜您获得了<span>888元现金红包</span></li>';
                    }
                    if(data.list[index].prizeType == 'JD'&&data.list[index].prizeValue == '500'){
                        str+='<li>恭喜您获得了<span>500元京东E卡</span></li>';
                    }
                    if(data.list[index].prizeType == 'JD'&&data.list[index].prizeValue == '1000'){
                        str+='<li>恭喜您获得了<span>1000元京东E卡</span></li>';
                    }
                    if(data.list[index].prizeType == 'JD'&&data.list[index].prizeValue == '5000'){
                        str+='<li>恭喜您获得了<span>5000元京东E卡</span></li>';
                    }
                    if(data.list[index].prizeType == 'LP'){
                        if(data.list[index].prizeValue == '网易严选28寸拉杆箱礼品资格'){
                            str+='<li>恭喜您达到了<span>'+data.list[index].prizeValue+'</span></li>'
                        }else{
                            str+='<li>恭喜您获得了<span>'+data.list[index].prizeValue+'</span></li>';
                        }

                    }
                });
                $('.hasreward ul').append(str);
                $('.hasreward').show();
            }else{
                $('.hasreward').hide();
                $('.noreward').removeClass('hide');
            }
       },
       error:function () {

       }
   })
});