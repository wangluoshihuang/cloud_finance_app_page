$(function(){
    var purchase = new Cloud();
    var data = decodeURI(purchase.getUrlParam('type'));
    if(data == null || data == ""){
        data = "info";
    }
    var screenHeight = $("body").height();
     $(".nav-item").each(function(){
         if(data == $(this).attr("data")){
             if(data == "safe"){
                document.title = "安全保障";
            }else{
                document.title = "了解云端";
            }
             $(".iframe").hide();
             $("#"+data).show();
             window.scrollTo(0,0);
             $(this).addClass("active");
         }else{
             $(this).removeClass("active");
         }
     });
    $(".nav-item").bind("tap",function(){
        $(".nav-item").removeClass("active");
        $(this).addClass("active");
        data = $(this).attr("data");
        $(".iframe").hide();
        if(data == "safe"){
            document.title = "安全保障";
        }else{
            document.title = "了解云端";
        }
        
        $("#"+data).show();
        window.scrollTo(0,0);
    });
    $(".tx").bind("tap",function(){
        $(".tx").removeClass("button-active");
        $(this).addClass("button-active");
        var c = $(this).attr("data");

        $(".item1-img").attr("class","item1-img "+c);

    })
    $(window).scroll(function() {
        if ($(window).scrollTop() > 60) {
             $("#info .btn-1").removeClass("btn-on").addClass("btn-off");
             $("#info .btn-1").attr("id","off");
             $("#info .btn-1").html("");
        } else {
             $("#info .btn-1").removeClass("btn-off").addClass("btn-on");
             $("#info .btn-1").attr("id","on");
             $("#info .btn-1").html("了解详情");
        }
    });
    $("#info .banner").css("height",screenHeight-164);
    $("#info .btn-1").bind("click",function(){
        var d = $(this).attr("id");
        if(d == "on"){
             window.scrollTo(0,screenHeight-164);
        }else{
              window.scrollTo(0,0);
        }

    })
    $("#info #yyzz").bind("tap",function(){
        
        if($(this).width() == 297){
            $(this).css("width",665);
            $(this).css("margin-top",-250);
        }else{
             $(this).css("width",297);
             $(this).css("margin-top",230);
        }
    })
})
