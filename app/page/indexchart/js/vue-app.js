var app = new Vue({
    el:'#indexchart',
    data:{
        result:{}
    },
    created: function () {
        $("#indexchart").show();
        this.init();   

    },
    methods: {
        //页面初始化
        init:function(){
            var _this = this;
            $.loading();
            $.ajax({
                url: "http://manage.yd-jr.cn/ydmgmt/api/public/queryIndexStatis",
                type: "get",
                dataType:'json',
                success:function(data){
                    $.loaded();
                    _this.result= data.result;

                        
                },
                error:function(er){
                     $.loaded();
                   
                }
            });
        }
    }
 });
