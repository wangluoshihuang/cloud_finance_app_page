/**
 * Created by zhangsan on 16/9/6.
 */
var Cloud=function(){
    this.isLogin=false;
    this.now=0;
    this.sTime=0;
    this.eTime=0;
    this.token=false;
    this.Root="";
    this.Place=false;
    this.pageRoot="";
    this.apiRoot="";
    this.manageRoot=""
    this.hadminRoot="";
    this.localRoot ="";
    this.iosURL="";
    this.androidURL="";
    this.androidWechat1URL="";
    this.shareRoot="";
    //this.iphoneSchema = 'wxb30ff5425cc67bbc://';
    this.iphoneSchema = 'cn.yd-jr.app://';
    this.androidSchema = 'cn.ydjf.app://';
    this.JavaRoot = "";
    this.AdminRoot = "";
    this.ActivityRoot = "";
    this.init();

};
Cloud.prototype.init=function () {
    this.setRoot();
    this.setPlace();
    //获取原生系统参数
    // this.getparameter();
    this.token = this.getUrlParam("data");
    this.iosURL="http://a.app.qq.com/o/simple.jsp?pkgname=cn.ydjr.app";
    this.androidURL = "https://cdn.yd-jr.cn//ydjr/php_project/apk/20170124/app-release-23-2.1.2-ydjr.apk";
    this.androidWechat1URL="https://cdn.yd-jr.cn/ydjr/php_project/apk/20161129/app-release-15-1.1.0-gzh001.apk";
};
//获取原生参数方法
Cloud.prototype.getparameter=function (CallBack) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.systemparameter.postMessage('');
        // top.location.href = this.Root+"Wap/app_skip/toFinish";
        //获取ios原生系统数据
        window.getsystemparameter = function (parameter) {
            var params= JSON.parse(parameter)
            var loginstatus = params.loginstatus
            window.isloginstatus = loginstatus
            window.systemdata = params
            CallBack(window.systemdata, window.isloginstatus)
        }
    }
    if(place==2){
        var b = window.stlc.systemparameter();
        var params= JSON.parse(b)
        var islogin = params.status
        window.isloginstatus = islogin
        window.systemdata = params
        CallBack(window.systemdata, window.isloginstatus)
    }
};
Cloud.prototype.setPlace=function () {
    var place=-1;
    if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
        if (navigator.userAgent.indexOf("MicroMessenger") > -1 || navigator.userAgent.indexOf("QQ") > -1 || navigator.userAgent.indexOf("Safari") > -1) {
            place=0;
        }else{
            place=1;
        }
    }else if(/(Android)/i.test(navigator.userAgent)){
        //产品详情页面
        if (navigator.userAgent.indexOf("MicroMessenger") > -1 || navigator.userAgent.indexOf("QQ") > -1) {
            place="0";
        }else{
            if(window.stlc){
                if(window.stlc.toMain){
                    place=2;
                }
            }else {
                place=0;
            }
        }
    }else{
        //wap
        // top.location.href =WAP_ROOT+"/invitation";
        place=0;
    }
    this.Place=place;
};
Cloud.prototype.getPlace=function () {
    return setPlace();
}
Cloud.prototype.setRoot=function () {
    var root="",pageRoot="",shareRoot="",apiRoot="",hadminRoot = "",manageRoot,javaRoot="",localRoot="",adminRoot="",activityRoot="",
        hrefStr=window.location.href;
    if(hrefStr.indexOf("h5.test.yd-jr.cn/")>-1){
        //root="http://wap.test.yd-jr.cn/";
        root="https://wap2.yd-jr.cn/";
        pageRoot="http://h5.test.yd-jr.cn/";
        shareRoot="http://h5.test.yd-jr.cn/";
        apiRoot="https://api.test.yd-jr.cn/";
        hadminRoot="http://114.55.246.10:8088/";
        manageRoot="http://manage.test.yd-jr.cn/";
        adminRoot = "http://admin.test.yd-jr.cn/";
        javaRoot="https://api.test.yd-jr.cn/";
        localRoot = "http://h5.test.yd-jr.cn/com/page/";
        activityRoot="https://apih5.dev.yd-jr.cn/api/";

    }else if(hrefStr.indexOf("h5.yd-jr.cn/")>-1){
        root="https://wap.yd-jr.cn/";
        pageRoot="https://h5.yd-jr.cn/";
        shareRoot="https://h5.yd-jr.cn/";
        apiRoot="https://wap.yd-jr.cn/";
        hadminRoot="http://hadmin.yd-jr.cn/";
        javaRoot = "https://api.yd-jr.cn/";
        adminRoot = "http://admin.yd-jr.cn/";
        localRoot = "https://h5.yd-jr.cn/com/page/";
        activityRoot="https://apih5.yd-jr.cn/api/";
    }else if(hrefStr.indexOf("open.yd-jr.cn/")>-1){
        manageRoot="http://manage.yd-jr.cn/";
    }else if(hrefStr.indexOf("h5.yufa.yd-jr.cn/")>-1){
        root="http://wap.yufa.yd-jr.cn/";
        pageRoot="http://h5.yufa.yd-jr.cn/";
        shareRoot="http://h5.yufa.yd-jr.cn/";
        apiRoot="https://java.yufa.yd-jr.cn/";
        hadminRoot="http://114.55.246.10:8088/";
        manageRoot="http://manage.test.yd-jr.cn/";
        adminRoot = "http://admin.yufa.yd-jr.cn/";
        javaRoot="https://java.yufa.yd-jr.cn/";
        localRoot = "http://h5.yufa.yd-jr.cn/com/page/";
        activityRoot="https://api.yufa.yd-jr.cn/api/";
    }
    else{
        root="https://wap2.yd-jr.cn/";
        pageRoot="http://h5.test.yd-jr.cn/";
        shareRoot="http://h5.test.yd-jr.cn/";
        apiRoot="https://api.test.yd-jr.cn/";
        javaRoot="https://api.test.yd-jr.cn/";
        hadminRoot="http://114.55.246.10:8088/";
        manageRoot="http://manage.test.yd-jr.cn/";
        localRoot = "http://192.168.9.52:8088/app/page/";
        adminRoot = "http://admin.test.yd-jr.cn/";
        activityRoot="https://apih5.dev.yd-jr.cn/api/";
        // root="http://wap.yufa.yd-jr.cn/";
        // pageRoot="http://h5.yufa.yd-jr.cn/";
        // shareRoot="http://h5.yufa.yd-jr.cn/";
        // apiRoot="https://java.yufa.yd-jr.cn/";
        // hadminRoot="http://114.55.246.10:8088/";
        // manageRoot="http://manage.test.yd-jr.cn/";
        // adminRoot = "http://admin.yufa.yd-jr.cn/";
        // javaRoot="https://java.yufa.yd-jr.cn/";
        // localRoot = "http://192.168.9.52:8088/app/page/";
        // activityRoot="https://api.yufa.yd-jr.cn/api/";
    }
    this.Root=root;
    this.pageRoot=pageRoot;
    this.shareRoot=shareRoot;
    this.JavaRoot = javaRoot;
    this.apiRoot=apiRoot;
    this.hadminRoot=hadminRoot;
    this.manageRoot=manageRoot;
    this.localRoot = localRoot;
    this.AdminRoot = adminRoot;
    this.ActivityRoot=activityRoot;
    return {root:root,pageRoot:pageRoot,apiRoot:apiRoot,hadminRoot:hadminRoot,manageRoot:manageRoot,javaRoot:javaRoot,localRoot:localRoot,adminRoot:adminRoot,activityRoot:activityRoot};
};
Cloud.prototype.getRoot=function() {
    return Cloud.prototype.setRoot();
}
Cloud.prototype.on=function (type,options) {
    var cid=Cloud.prototype.getUrlParam("cid");
    switch (type) {
        case "openPop":
            break;
        case "getInfo":
            return this.getInfo(cid);
            break;
        case "isAct":
            return this.isAct(options);
            break;
        case "toTickets":
            this.toTickets();
            break;
        case "goLogin":
            this.goLogin();//登录
            break;
        case "goIndex":
            this.goIndex();//首页
            break;
        case "goProduct":
            this.goProduct();//存钱罐
            break;
        case "goProductList":
            this.goProductList();//标列表
            break;
        case "goBuy":
            this.goBuy();//存钱罐购买
            break;
        case "goPro":
            this.goPro(options.id);//标详情
            break;    
        case "goShare":
            this.goShare(options.content);//分享
            break;
        case "goProfit":
            this.goProfit();//收支明细
            break;
        case "goBuyRecord":
            this.goBuyRecord();//购买记录
            break;
        case "goBuyPackage":
            this.goBuyPackage();  //券包
            break;  
        case "goWalletRecord":
            this.goWalletRecord();//存钱罐记录
            break;
        case "toFinish":
            this.toFinish();//返回到客户端
            break; 
        case "goShare":
            this.goShareCode(options.content);//分享二维码
            break;
        case "exitPop":
            this.exitPop();//退出测评弹窗
            break; 
        case "buylist":
            this.buylist(options.id);
            break; 
        case "unFinish":
            this.unFinish();//返回到客户端
            break; 
            

    }
};
Cloud.prototype.goLogin=function () {
    var place=this.Place;
    var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/6";
    }
    if(place==2){
        window.stlc.toLogin();
    }
};
Cloud.prototype.goIndex = function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/0";
    }
    if(place==2){
        window.stlc.toMain(0);
    }
};
Cloud.prototype.getInterent=function () {
    var place=this.Place;
    var result=0;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/0";
    }
    if(place==2){
        result= window.stlc.getInterent();
    }
    return result;
};
Cloud.prototype.goBuyPackage=function () {
    var place=this.Place;
    var result=0;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/7";
    }
    if(place==2){

        result= window.stlc.toTicketList();
    }
    return result;
};

//回调方法
Cloud.prototype.getCallBack=function(data){
    data=JSON.stringify(data);
    var place=this.Place;
    var result="";
    //如果是ios
    if(place==1){
        stlc.getCallBack(data);
    }
    // 如果是android
    if(place==2){
        result = window.stlc.getCallBack(data);
    }
    return result;
}

Cloud.prototype.goProductList=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/2";
    }
    if(place==2){
        window.stlc.toMain(1);
    }
};
Cloud.prototype.goProduct=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/3";
    }
    if(place==2){
        window.stlc.toWalletDetail();
    }
};
Cloud.prototype.goPro=function (id) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href ="byRegular:projectId:"+id;
    }
    if(place==2){
        window.stlc.toProDetail(id*1);
    }
};
Cloud.prototype.goBuy=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href ="stlc:buy:";
    }
    if(place==2){
        window.stlc.buy();
    }
};
//进立即出借页面
Cloud.prototype.goloan=function (result) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        
    }
    if(place==1){
        var data = {
           id:result.id
        }
        window.webkit.messageHandlers.Immediatelylend.postMessage(data);
    }
    if(place==2){
        window.stlc.toInvest(JSON.stringify(result));
    }
};
Cloud.prototype.goShare=function (content) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
       // alert("ios stlc:share:");
        top.location.href ="stlc:share:"+JSON.stringify(content);
    }
    if(place==2){
       // alert("android share(content)");
        window.stlc.share(JSON.stringify(content));
    }
};
Cloud.prototype.goProfit=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/paymentDetails";
    }
    if(place==2){
    	
        window.stlc.toWalletIncome();
    }
};
Cloud.prototype.goBuyRecord=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/investRecords";
    }
    if(place==2){
        window.stlc.toUerBuyRecords();
    }
};
Cloud.prototype.goWalletRecord=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/paymentDetails";
    }
    if(place==2){
        window.stlc.toWalletIncome();
    }
};
Cloud.prototype.toFinish=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.toFinish.postMessage('');
        // top.location.href =this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toFinish();
    }
};
Cloud.prototype.unFinish=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.abnormalexit.postMessage('');
        // top.location.href =this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toFinish();
    }
};
Cloud.prototype.exitPop=function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.bouncedaway.postMessage('');
    }
    if(place==2){
        window.stlc.dismissDialog();
    }
};
Cloud.prototype.buylist=function (id) {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        window.webkit.messageHandlers.buylist.postMessage(id);
    }
    if(place==2){
        window.stlc.toInvestLog(id.id);
    }
};
//隐藏ios 安卓  tabbar
Cloud.prototype.goproductdetail=function (url) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.hidethetabbar.postMessage(url);
        // top.location.href = this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toProjectDetail(url.url);
    }
};
// 由原生转发获取的参数
Cloud.prototype.gettransmitdata=function (data,CallBack) {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        window.webkit.messageHandlers.postH5UniversalInterface.postMessage(data);
        window.getinterfacedata = function (parameter) {
            var params= JSON.parse(parameter)
            if(typeof CallBack == 'function'){
                CallBack(params)
            }
        }
    }
    if(place==2){
        var b = window.stlc.postH5UniversalInterface(data);
        var params= JSON.parse(b)
        CallBack(params)
    }
};
//二级页面
Cloud.prototype.goprodetail=function (url) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.hidethetabbar.postMessage(url);
        // top.location.href = this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toProjectDetailsubpage(url.url);
    }
};
Cloud.prototype.goFriend=function (url) {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.BaseWebView.postMessage(url);
        // top.location.href = this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toInviteFriends(url.url);
    }
};
//显示ios 安卓 tabbar
Cloud.prototype.backproductdetail= function () {
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
        window.webkit.messageHandlers.displaytabBar.postMessage('');
        // top.location.href =this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toFinish();
    }
};
Cloud.prototype.goShareCode=function (content) {
	//alert(content)
    var place=this.Place;
    // var myUrl=window.location.href;
    if(place==0){
        // top.location.href =this.Root+"/invitation?source="+myUrl;
    }
    if(place==1){
    //    alert("ios stlc:share:");
       
       var con = JSON.stringify(content);
       window.webkit.messageHandlers.shareCode.postMessage(con);
        // top.location.href ="stlc:shareCode:"+JSON.stringify(content);
    }
    if(place==2){
       // alert("android share(content)");
        window.stlc.shareCode(JSON.stringify(content));
    }
};
Cloud.prototype.isAct=function (params,testActNo) {
    var getInfoParam=params;
    var sTime=getInfoParam.sTime,
        eTime=getInfoParam.eTime,
        now=getInfoParam.now;
    var startTime=new Date(sTime*1000).getTime(),
        endTime=new Date(eTime*1000).getTime(),
        nowTime=new Date(now*1000).getTime();
    if(!testActNo&&testActNo!=0){
        var isA=-1;
        //判断是否开始  按钮显示
        if(nowTime<startTime){
            isA=-1;
        }
        if(nowTime>=startTime&&nowTime<endTime){
            isA=1;
        }
        if(nowTime>endTime){
            isA=0;
        }
    }else {
        isA=testActNo;
    }
    return isA;
};

Cloud.prototype.getUrlParam=function (param) {
        var myUrl=window.location.href,
            tId="";
        if(myUrl.indexOf("?")>-1){
            var myUrlParam=myUrl.split("?")[1];
            if(myUrlParam.indexOf("&")>-1){
                var tIdArr=myUrlParam.split("&");
                for(var i=0;i<tIdArr.length;i++){
                    if(tIdArr[i].split("=")[0]==param){
                        tId=tIdArr[i].split("=")[1];
                        break;
                    }
                }
            }else {
                if(myUrlParam.split("=")[0]==param){
                    tId=myUrlParam.split("=")[1];
                }
            }
        }
        return tId;
}
//获取url中的参数
Cloud.prototype.getUrlParam2=function(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}
Cloud.prototype.download =  function(type){
    var iosurl=this.iosURL;
    var aurl = this.androidURL;
    var burl = this.androidWechat1URL;

    //微信
    if(window.mobileUtil.isAndroid && window.mobileUtil.isWeixin){
        $("#bg").show();
        $("#show").show();
    }else{//非微信浏览器
        if (window.mobileUtil.isIos) {
                window.location.href = iosurl;
        }else if (window.mobileUtil.isAndroid) {
            if(type == 'down_wechat1.html'){
                window.location=burl;
            }else{
                $.loading();
                $.ajax({
                    type:"get",
                    url:"https://wap.yd-jr.cn/common/androidUrl.json",
                    async:false,
                    success:function (data) {
                        $.loaded();
                        if(data.status==1){
                            aurl=data.url;
                        }
                        window.location=aurl;
                    },
                    error:function (data) {
                        window.location=aurl;
                    },
                    dataType:"json",
                });

            }
        }
    }
}

//手机号验证
Cloud.prototype.checkTel = function (param){
    var reg = /^0?1[0-9][0-9]\d{8}$/;
    return reg.test(param);
}
Cloud.prototype.formatDate1 = function () {
    var dat = new Date();
    var y = dat.getFullYear();
    var m = dat.getMonth()+1;
    m = m<10 ? ('0' + m):m;
    var d = dat.getDate();
    d = d < 10 ? ('0' + d):d;
    var h = dat.getHours();
    h = h < 10 ? ('0' + h):h;
    console.log(y+m+d+h)
    return y+m+d+h;
};
Cloud.prototype.formatDate2 = function (timeinterval) {
    var dat = new Date(timeinterval);
    var y = dat.getFullYear();
    var m = dat.getMonth()+1;
    m = m<10 ? ('0' + m):m;
    var d = dat.getDate();
    d = d < 10 ? ('0' + d):d;
    var h = dat.getHours();
    h = h < 10 ? ('0' + h):h;
    var minute = dat.getMinutes();
    minute = minute < 10 ? ('0' + minute):minute;
    var s = dat.getSeconds();
    s = s < 10 ? ('0' + s):s;
    return h + ":" + minute + ":" + s;
};
//获取ios原生系统数据
// window.getsystemparameter = function (parameter) {
//    var params= JSON.parse(parameter)
//    var loginstatus = params.loginstatus
//    window.isloginstatus = loginstatus
//    window.systemdata = parameter
//    alert(window.systemdata)
// }
Cloud.prototype.getUrlParam3=function(key) {
    var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
    var result = window.location.search.substr(1).match(reg);
    return result?decodeURIComponent(result[2]):null;
}