function URLEncode(Str){
    if(Str==null||Str=="")
     return "";
    var newStr="";
    function toCase(sStr){
     return sStr.toString(16).toUpperCase();
     }
    for(var i=0,icode,len=Str.length;i<len;i++){
     icode=Str.charCodeAt(i);
     if( icode<0x10)
      newStr+="%0"+icode.toString(16).toUpperCase();
     else if(icode<0x80){
      if(icode==0x20)
       newStr+="+";
      else if((icode>=0x30&&icode<=0x39)||(icode>=0x41&&icode<=0x5A)||(icode>=0x61&&icode<=0x7A))
       newStr+=Str.charAt(i);
      else
       newStr+="%"+toCase(icode);
      }
     else if(icode<0x800){
      newStr+="%"+toCase(0xC0+(icode>>6));
      newStr+="%"+toCase(0x80+icode%0x40);
      }
     else{
      newStr+="%"+toCase(0xE0+(icode>>12));
      newStr+="%"+toCase(0x80+(icode>>6)%0x40);
      newStr+="%"+toCase(0x80+icode%0x40);
      }
     }
    return newStr;
    }