var _mtac = {"performanceMonitor":1};
(function(win, doc){
    //配置腾讯统计埋点
    //var _mtac = {"senseHash":0,"autoReport":0};
    var mta = document.createElement("script");
    mta.src = "https://pingjs.qq.com/h5/stats.js?v2.0.4";
    mta.setAttribute("name", "MTAH5");
    mta.setAttribute("sid", "500386442");
    mta.setAttribute("cid", "500386553");
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(mta, s);

    var tajs  = document.createElement("script");
    tajs.src = "https://tajs.qq.com/stats";
    tajs.setAttribute("sId", "59614892");
    var ts = document.getElementsByTagName("script")[0];
    ts.parentNode.insertBefore(tajs, ts);
    //移动端配置meta
    var phoneScale = parseInt(window.screen.width)/1080;
    var oMeta = document.createElement('meta');
    oMeta.charset = 'utf-8';
    oMeta.name ="viewport";
    oMeta.content = 'user-scalable=no,width=1080, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', 　　target-densitydpi=device-dpi';
    document.getElementsByTagName('head')[0].appendChild(oMeta);
})(window, document);
