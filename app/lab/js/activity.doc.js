
(function ($) {
    $._global={};
        $.loading = function(b) {
            var c = b || {
                        imgUrl: "../../lab/img/07.gif",
                        width: "50px",
                        height: "50px"
                    },
                body = document.body,     
                d = document.createElement("div"),
                e = document.createElement("i");

            e.style.background = "url('" + c.imgUrl + "') no-repeat center center",
                e.style.backgroundSize = "100%",
                e.style.display = "inline-block",
                e.style.width = c.width,
                e.style.height = c.height,
                e.style.verticalAlign = "middle",
                d.style.position = "fixed",
                d.style.left = "0",
                d.style.top = "45%",
                d.style.width = "100%",
                d.style.textAlign = "center",
                d.id="page-loading",
                
                d.appendChild(e),
                body.appendChild(d),
                body.overflow="hidden";
        }  
        //加载完之后
	   $.loaded = function() {
            var el =  document.getElementById('page-loading');
            el.parentNode.removeChild(el);
            body = document.body;
	        body.overflow="auto";
	    }
      $.openPop = function(opt){

             opt={
                title:opt.title,
                btnTextNo:opt.btnTextNo||"知道了",
                btnText:opt.btnText||"知道了",
                btnGo:opt.btnGo||function(){},
                btnClose:opt.btnClose||function(){},
                tc:opt.tc||"j_err"
            };
           if($('.'+opt.tc).text() == ''){
           
                if($('.zz').text() == ''){
                    $('<div class="zz">').appendTo($('body'));
                }
                var view={
                    j_err:'<div class="tanchuang j_err" id="j_err">'
                    +'<div class="tc-middle">'
                    +'<div class="tc-cont"><p class="tc-title">'+opt.title+'</p></div>'
                    +'<div class="ts1">'+opt.btnTextNo+'</div>'
                    +'</div> </div>',
                    tc_A:'<div class="tanchuang1 tanchuang tc_A">'
                    +'<div class="tc-middle">'
                    +'<div class="tc-cont"><p class="tc-title">'+opt.title+'</p></div>'
                    +'<div class="tc">'
                    +'<div class="ts1">'+opt.btnTextNo+'</div>'
                    +'<span></span>'
                    +'<div class="ts2">'+opt.btnText+'</div>'
                    +'</div>'
                    +'</div>'
                    +'</div>'
                }
                        
                $(view[opt.tc]).appendTo($('body'));
           }
            $("body").css("overflow","hidden");
            $(".zz,."+opt.tc).show();
            if($(".ts2").text() != ''){
                $(".ts2").unbind();
                $(".ts2").bind("tap",function(){
                    $("body").css("overflow","auto");
                    if(opt.btnGo){
                        opt.btnGo();
                    }
                    $(".zz,."+opt.tc).hide();
                })
            }
            
            $(".ts1").click(function(){
                $("body").css("overflow","auto");
                if(opt.btnClose){
                    opt.btnClose();
                }
                $(".zz,.tanchuang").hide();
            })
        }
        /**
     * 提示窗
     *
     * **/
    $.popupTxt=function(id,params,timer){
        paramsObj=params || "<span>请填写文字</span>";
        parentId=id;
        var div;
        var parant=document.body;
        if(parentId !=null){
            parant=document.getElementById(parentId);
        }
        if(document.getElementById("message") == undefined){
            div = document.createElement("div");
            div.id = "message";
            if(parant.children[0]){
            　　　　parant.insertBefore(div,parant.children[0]);
            　　}else{
                　 parant.appendChild(div);　　
            　　}
        }else{
            div = document.getElementById("message");
        }

        div.innerHTML = paramsObj;
        div.style.display="block";
        clearTimeout($._global.timer);
        $._global.timer=setTimeout(function(){
            parant.removeChild(div);
        },timer);
    }
  
})(jQuery);