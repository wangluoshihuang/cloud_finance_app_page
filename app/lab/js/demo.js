/**
 * Created by zhangsan on 16/9/6.
 */
var yd=function(){
    this.isLogin=false;
    this.now=0;
    this.sTime=0;
    this.eTime=0;
    this.token=false;
    this.Root="";
    this.Place=false;
    this.pageRoot="";
    this.apiRoot="";
    this.manageRoot=""
    this.hadminRoot="";
    this.iosURL="";
    this.androidURL="";
    this.androidWechat1URL="";
    this.shareRoot="";
    //this.iphoneSchema = 'wxb30ff5425cc67bbc://';
    this.iphoneSchema = 'cn.yd-jr.app://';
    this.androidSchema = 'cn.ydjf.app://';
    this.init();
};
yd.prototype.init=function () {
    this.setRoot();
    this.setPlace();
    this.token=this.getUrlParam("data");
    this.iosURL="http://a.app.qq.com/o/simple.jsp?pkgname=cn.ydjr.app";
    this.androidURL = "https://cdn.yd-jr.cn//ydjr/php_project/apk/20170124/app-release-23-2.1.2-ydjr.apk";
    this.androidWechat1URL="https://cdn.yd-jr.cn/ydjr/php_project/apk/20161129/app-release-15-1.1.0-gzh001.apk";
};

yd.prototype.setPlace=function () {
    var place=-1;
    if(/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)){
        if (navigator.userAgent.indexOf("MicroMessenger") > -1 || navigator.userAgent.indexOf("QQ") > -1 || navigator.userAgent.indexOf("Safari") > -1) {
            place=0;
        }else{
            place=1;
        }
    }else if(/(Android)/i.test(navigator.userAgent)){
        if (navigator.userAgent.indexOf("MicroMessenger") > -1 || navigator.userAgent.indexOf("QQ") > -1) {
            place="0";
        }else{
            if(window.stlc){
                if(window.stlc.toMain){
                    place=2;
                }
            }else {
                place=0;
            }
        }
    }else{
        place=0;
    }
    this.Place=place;
};
yd.prototype.setRoot=function () {
    var root="",pageRoot="",shareRoot="",apiRoot="",hadminRoot = "",manageRoot,
        hrefStr=window.location.href;
    if(hrefStr.indexOf("h5.test.yd-jr.cn/")>-1){
        root="https://wap2.yd-jr.cn/";
        pageRoot="http://h5.test.yd-jr.cn/";
        shareRoot="http://h5.test.yd-jr.cn/";
        apiRoot="https://api.test.yd-jr.cn/";
        hadminRoot="http://114.55.246.10:8088/";
        manageRoot="http://manage.test.yd-jr.cn/";

    }else if(hrefStr.indexOf("h5.yd-jr.cn/")>-1){
        root="https://wap.yd-jr.cn/";
        pageRoot="https://h5.yd-jr.cn/";
        shareRoot="https://h5.yd-jr.cn/";
        apiRoot="https://wap.yd-jr.cn/";
        hadminRoot="http://hadmin.yd-jr.cn/";
    }else if(hrefStr.indexOf("open.yd-jr.cn/")>-1){
        manageRoot="http://manage.yd-jr.cn/";
    }
    else{
        root="https://wap2.yd-jr.cn/";
        pageRoot="http://h5.test.yd-jr.cn/";
        shareRoot="http://h5.test.yd-jr.cn/";
        apiRoot="https://api.test.yd-jr.cn/";
        hadminRoot="http://114.55.246.10:8088/";
        manageRoot="http://manage.test.yd-jr.cn/";
    }
    this.Root=root;
    this.pageRoot=pageRoot;
    this.shareRoot=shareRoot;
    this.apiRoot=apiRoot;
    this.hadminRoot=hadminRoot;
    this.manageRoot=manageRoot;
};
yd.prototype.on=function (type,options) {
    var cid=yd.prototype.getUrlParam("cid");
    switch (type) {
        case "openPop":
            break;
        case "getInfo":
            return this.getInfo(cid);
            break;
        case "isAct":
            return this.isAct(options);
            break;
        case "toTickets":
            this.toTickets();
            break;
        case "goLogin":
            this.goLogin();//登录
            break;
        case "goIndex":
            this.goIndex();//首页
            break;
        case "goProduct":
            this.goProduct();//存钱罐
            break;
        case "goProductList":
            this.goProductList();//标列表
            break;
        case "goBuy":
            this.goBuy();//存钱罐购买
            break;
         case "goPro":
            this.goPro(options.id);//标详情
            break;    
        case "goShare":
            this.goShare(options.content);//分享
            break;
        case "goProfit":
            this.goProfit();
            break;
        case "goBuyRecord":
            this.goBuyRecord();//购买记录
            break;
        case "goBuyPackage":
            this.goBuyPackage();  //券包
            break;  
        case "goWalletRecord":
            this.goWalletRecord();//存钱罐记录
            break;
        case "toFinish":
            this.toFinish();//返回到客户端
            break; 
        case "goShare":
            this.goShareCode(options.content);//分享二维码
            break;  

    }
};
yd.prototype.goLogin=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/6";
    }
    if(place==2){
        window.stlc.toLogin();
    }
};
yd.prototype.goIndex=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/0";
    }
    if(place==2){
        window.stlc.toMain(0);
    }
};
yd.prototype.getInterent=function () {
    var place=this.Place;
    var result=0;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/0";
    }
    if(place==2){
        window.stlc.getInterent();
    }
};
yd.prototype.goBuyPackage=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/7";
    }
    if(place==2){
        window.stlc.toTicketList();
    }
};

//回调方法
yd.prototype.getCallBack=function(data){
    data=JSON.stringify(data);
    var place=this.Place;
    //如果是ios
    if(place==1){
        stlc.getCallBack(data);
    }
   // 如果是android
    if(place==2){
        window.stlc.getCallBack(data);
    }
}

yd.prototype.goProductList=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/2";
    }
    if(place==2){
        window.stlc.toMain(1);
    }
};
yd.prototype.goProduct=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/status/3";
    }
    if(place==2){
        window.stlc.toWalletDetail();
    }
};
yd.prototype.goPro=function (id) {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href ="byRegular:projectId:"+id;
    }
    if(place==2){
        window.stlc.toProDetail(id*1);
    }
};
yd.prototype.goBuy=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href ="stlc:buy:";
    }
    if(place==2){
        window.stlc.buy();
    }
};
yd.prototype.goShare=function (content) {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href ="stlc:share:"+JSON.stringify(content);
    }
    if(place==2){
        window.stlc.share(JSON.stringify(content));
    }
};
yd.prototype.goProfit=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/paymentDetails";
    }
    if(place==2){
        window.stlc.toWalletIncome();
    }
};
yd.prototype.goBuyRecord=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/investRecords";
    }
    if(place==2){
        window.stlc.toUerBuyRecords();
    }
};
yd.prototype.goWalletRecord=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        top.location.href =this.Root+"Wap/app_skip/paymentDetails";
    }
    if(place==2){
        window.stlc.toWalletIncome();
    }
};
yd.prototype.toFinish=function () {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
        window.webkit.messageHandlers.toFinish.postMessage('');
        // top.location.href =this.Root+"Wap/app_skip/toFinish";
    }
    if(place==2){
        window.stlc.toFinish();
    }
};
yd.prototype.goShareCode=function (content) {
    var place=this.Place;
    if(place==0){
    }
    if(place==1){
       var con = JSON.stringify(content);
       window.webkit.messageHandlers.shareCode.postMessage(con);
        // top.location.href ="stlc:shareCode:"+JSON.stringify(content);
    }
    if(place==2){
        window.stlc.shareCode(JSON.stringify(content));
    }
};
yd.prototype.isAct=function (params,testActNo) {
    var getInfoParam=params;
    var sTime=getInfoParam.sTime,
        eTime=getInfoParam.eTime,
        now=getInfoParam.now;
    var startTime=new Date(sTime*1000).getTime(),
        endTime=new Date(eTime*1000).getTime(),
        nowTime=new Date(now*1000).getTime();
    if(!testActNo&&testActNo!=0){
        var isA=-1;
        //判断是否开始  按钮显示
        if(nowTime<startTime){
            isA=-1;
        }
        if(nowTime>=startTime&&nowTime<endTime){
            isA=1;
        }
        if(nowTime>endTime){
            isA=0;
        }
    }else {
        isA=testActNo;
    }
    return isA;
};

yd.prototype.getUrlParam=function (param) {
        var myUrl=window.location.href,
            tId="";
        if(myUrl.indexOf("?")>-1){
            var myUrlParam=myUrl.split("?")[1];
            if(myUrlParam.indexOf("&")>-1){
                var tIdArr=myUrlParam.split("&");
                for(var i=0;i<tIdArr.length;i++){
                    if(tIdArr[i].split("=")[0]==param){
                        tId=tIdArr[i].split("=")[1];
                        break;
                    }
                }
            }else {
                if(myUrlParam.split("=")[0]==param){
                    tId=myUrlParam.split("=")[1];
                }
            }
        }
        return tId;
}
//获取url中的参数
yd.prototype.getUrlParam2=function(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}
yd.prototype.download =  function(type){
    var iosurl=this.iosURL;
    var aurl = this.androidURL;
    var burl = this.androidWechat1URL;
    //微信
    if(window.mobileUtil.isAndroid && window.mobileUtil.isWeixin){
        $("#bg").show();
        $("#show").show();
    }else{//非微信浏览器
        if (window.mobileUtil.isIos) {
                window.location.href = iosurl;
        }else if (window.mobileUtil.isAndroid) {
            if(type == 'down_wechat1.html'){
                window.location=burl;
            }else{
                $.loading();
                $.ajax({
                    type:"get",
                    url:"https://wap.yd-jr.cn/common/androidUrl.json",
                    async:false,
                    success:function (data) {
                        $.loaded();
                        if(data.status==1){
                            aurl=data.url;
                        }
                        window.location=aurl;
                    },
                    error:function (data) {
                        window.location=aurl;
                    },
                    dataType:"json",
                });

            }
        }
    }
}
//手机号验证
yd.prototype.checkTel = function (param){
    var reg = /^0?1[0-9][0-9]\d{8}$/;
    return reg.test(param);
};
//适配以及腾讯埋点设置
(function(win, doc){
    //配置腾讯统计埋点
    //var _mtac = {"senseHash":0,"autoReport":0};
    var mta = document.createElement("script");
    mta.src = "https://pingjs.qq.com/h5/stats.js?v2.0.4";
    mta.setAttribute("name", "MTAH5");
    mta.setAttribute("sid", "500386442");
    mta.setAttribute("cid", "500386553");
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(mta, s);

    var tajs  = document.createElement("script");
    tajs.src = "https://tajs.qq.com/stats";
    tajs.setAttribute("sId", "59614892");
    var ts = document.getElementsByTagName("script")[0];
    ts.parentNode.insertBefore(tajs, ts);
    //移动端配置meta
    var oMeta = document.createElement('meta');
    oMeta.charset = 'utf-8';
    oMeta.name ="viewport";
    oMeta.content = 'width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no';
    document.getElementsByTagName('head')[0].appendChild(oMeta);

    !function(e){var g=e,f=g.document,h=f.documentElement,a=f.getElementsByTagName("html")[0],d=h.clientWidth,c="fontSize",b=function(k,i,j){return k.style[i]=j+"px"};b(a,c,d/10.8);g.onresize=function(){var j=g.document,k=j.documentElement,l=j.getElementsByTagName("html")[0],i=k.clientWidth;b(l,c,i/10.8)}}(window);
})(window, document);