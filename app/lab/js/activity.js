
/**
 * Created by zhangsan on 16/9/6.
 */
;(function ($) {
    /**
     * touch事件兼容
     */
    // $(window).on("scroll",function (ev) {
    //     ev.stopPropagation();
    // });
        /**
         * 手机号码验证
         */
        $.phoneTest = /^(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])\d{8}$/i,
        $._global={};
        /**
         * 中文验证
         */
        $.wordTest = /[\u4E00-\u9FA5]/g,
        /**
         * 是否是微信环境
         */
             
        $.is_weixn = function() {
            var a = navigator.userAgent.toLowerCase();
            return "micromessenger" == a.match(/MicroMessenger/i)
        },
        /**
         * 加载开始
         * {imgUrl:图片地址,width:宽度,height:高度}
         */
        $.loading = function(b) {
            var c = b || {
                        imgUrl: "../../lab/img/07.gif",
                        width: "50px",
                        height: "50px"
                    },
                d = document.createElement("div"),
                e = document.createElement("i");
            e.style.background = "url('" + c.imgUrl + "') no-repeat center center",
                e.style.backgroundSize = "100%",
                e.style.display = "inline-block",
                e.style.width = c.width,
                e.style.height = c.height,
                e.style.verticalAlign = "middle",
                d.style.position = "fixed",
                d.style.left = "0",
                d.style.top = "0",
                d.style.paddingTop = "45%",
                d.style.width = "100%",
                d.style.height = "100%",
                d.style.textAlign = "center",
                $(d).attr("id", "page-loading"),
                d.appendChild(e),
                $("body").append(d).css("overflow","hidden");
        }
        //加载完之后
	    $.loaded = function() {
	        $("#page-loading").remove()
	        $("body").css("overflow","auto");
	    }
        $.loadPage = function(){
            var d = document.createElement("div");
                d.style.background = "#fff",
                d.style.position = "fixed",
                d.style.left = "0",
                d.style.top = "0",
                d.style.width = "100%",
                d.style.height = window.screen.height + "px",
                d.style.lineHeight = window.screen.height + "px",
                d.style.textAlign = "center",
                $(d).attr("id", "page-loading1"),
                $("body").append(d).css("overflow","hidden");
        }
        $.loadPageEd = function() {
            $("#page-loading1").remove()
            $("body").css("overflow","auto");
        }
        $.openPop = function(opt){
             opt={
                title:opt.title,
                btnTextNo:opt.btnTextNo||"知道了",
                btnText:opt.btnText||"知道了",
                btnGo:opt.btnGo||function(){},
                btnClose:opt.btnClose||function(){},
                tc:opt.tc||"j_err"
            };

           if($('.'+opt.tc).text() == undefined){
                if($('.zz').text() == undefined){
                    $('<div class="zz">').appendTo($('body'));
                }
                var view={
                    j_err:'<div class="tanchuang j_err" id="j_err">'
                    +'<div class="tc-middle">'
                    +'<div class="tc-cont"><p class="tc-title">'+opt.title+'</p></div>'
                    +'<div class="ts1">'+opt.btnTextNo+'</div>'
                    +'</div> </div>',
                    tc_A:'<div class="tanchuang1 tanchuang tc_A">'
                    +'<div class="tc-middle">'
                    +'<div class="tc-cont"><p class="tc-title">'+opt.title+'</p></div>'
                    +'<div class="tc">'
                    +'<div class="ts1">'+opt.btnTextNo+'</div>'
                    +'<span></span>'
                    +'<div class="ts2">'+opt.btnText+'</div>'
                    +'</div>'
                    +'</div>'
                    +'</div>'
                }
                $(view[opt.tc]).appendTo($('body'));
           }else{
               $(".tc-title").html(opt.title);
           }
            $("body").css("overflow","hidden");
            $(".zz,."+opt.tc).show();
            if($(".ts2").text() != undefined){
                $(".ts2").unbind();
                $(".ts2").bind("tap",function(){
                    $("body").css("overflow","auto");
                    if(opt.btnGo){
                        opt.btnGo();
                    }
                    $(".zz,."+opt.tc).hide();
                })
            }
            
            $(".ts1").unbind();
            $(".ts1").bind("tap",function(){
                $("body").css("overflow","auto");
                if(opt.btnClose){
                    opt.btnClose();
                }
                $(".zz,.tanchuang").hide();
            })
        }

    /**
     * 提示窗
     *
     * **/
    $.popupTxt=function(id,params,timer){
        paramsObj=params || "<span>请填写文字</span>";
        parentId=id;
        var div;
        var parant=document.body;
        if(parentId !=null){
            parant=document.getElementById(parentId);
        }
        if(document.getElementById("message") == undefined){
            div = document.createElement("div");
            div.id = "message";
            if(parant.children[0]){
            　　　　parant.insertBefore(div,parant.children[0]);
            　　}else{
                　 parant.appendChild(div);　　
            　　}
        }else{
            div = document.getElementById("message");
        }

        div.innerHTML = paramsObj;
        div.style.display="block";
        clearTimeout($._global.timer);
        $._global.timer=setTimeout(function(){
            parant.removeChild(div);
        },timer);
    }
    /**
     *
     * 向上滑动
     * @param wrap  overflow hidden的父层
     * @param ul   滚动的ul
     */
    $.slideList=function(wrap,ul){
        var $wrap=$(wrap),$ul=$(ul);
        $wrap.css({"position":"relative"});
        setTimeout(function(){
            $ul.css({"transition":"top 1s ease-out 0s",
                "-webkit-transition":"top 1s ease-out 0s",
                "-moz-transition":"top 1s ease-out 0s",
                "-ms-transition":"top 1s ease-out 0s",
                "-o-transition":"top 1s ease-out 0s"});
        },500);
        var i = 0 , j = 0;
        var html=$ul.html();
        (function move(){
            window.requestAnimationFrame(move); 
            var $allLi= $ul.find("li"),
                $liHeight=$allLi.height();
            i++;
            var goTop=i/3;
            $ul.css({"transform":"translate3d(0,"+-goTop+"px,0)"});
            $ul.css({"-webkit-transform":"translate3d(0,"+-goTop+"px,0)"});
            $ul.css({"-moz-transform":"translate3d(0,"+-goTop+"px,0)"});
            $ul.css({"-ms-transform":"translate3d(0,"+-goTop+"px,0)"});
            $ul.css({"-o-transform":"translate3d(0,"+-goTop+"px,0)"});
           
            if($ul.height()-goTop-$wrap.height()<50){
                $ul.append(html);
                j++;
            }
        })();
    }
    $.AutoSize=function (ul) {
        var element = $(ul);
        auto();
        function auto() {
            var width = $(window).width(),
                height = $(window).height();
            $("html").css("font-size", width / 15);
            $(element).width(width).height(height);
        };
        $(window).resize(auto);
    }
})(Zepto);