
var pageRoot="";
var hrefStr=window.location.href;
if(hrefStr.indexOf("h5.test.yd-jr.cn/")>-1){
    pageRoot="http://h5.test.yd-jr.cn";
    

}else if(hrefStr.indexOf("h5.yd-jr.cn/")>-1){
   
    pageRoot="https://h5.yd-jr.cn";
   
}else if(hrefStr.indexOf("h52.yd-jr.cn/")>-1){
        
    pageRoot="https://h52.yd-jr.cn";
       
}else{
    
    pageRoot="http://h5.test.yd-jr.cn";
    
}
var data=[
    // {title:"暖春-发现",url:pageRoot+"/build/html/activity/activity_spring.html"},
    // {title:"暖春-banner",url:pageRoot+"/build/html/activity/activity_spring_banner.html"},
    // {title:"暖春-弹窗",url:pageRoot+"/build/html/activity/activity_spring_pop.html"},
    {title:"隐私协议",url:pageRoot+"/build/html/privacyText/index.html"},
    {title:"风险提示书",url:pageRoot+"/build/html/purchaseOrder/fx.html"},
    {title:"云端金融服务协议",url:pageRoot+"/build/html/purchaseOrder/xy.html"},
    {title:"借款协议",url:pageRoot+"/build/html/purchaseOrder/purchase_order.html"},
    {title:"支付服务协议",url:pageRoot+"/build/html/purchaseOrder/zx.html"},
    {title:"被邀请页",url:pageRoot+"/build/html/invitation/index.html?ydjrphone=135*******678&ydjrcode=e3oyfc84"},
    {title:"平台规则",url:pageRoot+"/build/html/rulePlatform/index.html"},
    {title:"关于我们",url:pageRoot+"/build/html/company/about.html"},
    {title:"媒体报道",url:pageRoot+"/build/html/company/media.html"},
    {title:"项目信息",url:pageRoot+"/build/html/projectInfo/index.html"},
    {title:"安全保障",url:pageRoot+"/build/html/projectInfo/safe.html"},
    // {title:"8888体验金",url:pageRoot+"/build/html/activity/activity_8888.html"},
    // {title:"阶梯加息",url:pageRoot+"/build/html/activity/activity_stair_1.html"},
    // {title:"好友邀请-首页",url:pageRoot+"/build/html/activity/activity_friend.html"},
    // {title:"好友邀请-详细规则",url:pageRoot+"/build/html/activity/activity_friend_detail.html"},
    // {title:"好友邀请-获奖列表",url:pageRoot+"build/html/activity/activity_friend_record.html"},
    {title:"私人定制-首页",url:pageRoot+"/com/page/friend/index.html"},
    {title:"私人定制-详细规则",url:pageRoot+"/com/page/friend/rule.html"},
    {title:"忠诚奖励",url:pageRoot+"/build/html/activity/activity_zc.html"},
    // {title:"抽奖",url:pageRoot+"/build/html/activity/activity_cj.html"},
    {title:"下载链接",url:pageRoot+"/build/html/invitation/down.html"},
    {title:"下载链接-短信",url:pageRoot+"/build/html/invitation/down_msg.html"},
    {title:"下载链接-微信",url:pageRoot+"/build/html/invitation/down_wechat.html"},
    {title:"下载链接-渠道",url:pageRoot+"/build/html/invitation/down_activity.html"},
    {title:"常见问题",url:pageRoot+"/build/html/commonProblem/common_problem.html"},
    // {title:"公告通知",url:pageRoot+"/build/html/company/notice.html"},
    {title:"注册-流量宝",url:pageRoot+"/build/html/invitation/registered_1.html"},
    {title:"注册-魔积分",url:pageRoot+"/build/html/invitation/registered_2.html"},
    {title:"注册-小米",url:pageRoot+"/build/html/invitation/registered_3.html"},
    {title:"了解云端",url:pageRoot+"/com/page/company/about.html"},
    {title:"A轮活动",url:pageRoot+"/com/page/financ/index.html"},
    {title:"云端金融A轮融资",url:pageRoot+"/com/page/company/notice_A.html"},
    {title:"新了解云端",url:pageRoot+"/com/page/safe/index.html?type=info"},
    {title:"银行存管",url:pageRoot+"/com/page/notice/notice_bank.html"},
     {title:"银行存管（新）",url:pageRoot+"/com/page/notice/notice_bank_new.html"},
    {title:"签约存管活动",url:pageRoot+"/com/page/bank_activity/index.html"},
    {title:"新手标升级活动",url:pageRoot+"/com/page/xsb_activity/index.html"},
    {title:"新邀友活动",url:pageRoot+"/com/page/friend_new/index.html"},
    {title:"七夕活动",url:pageRoot+"/com/page/qx_activity/index.html"},
    {title:"教师节活动",url:pageRoot+"/com/page/teacher_activity/index.html"},
    {title:"爱自己早餐计划",url:pageRoot+"/com/page/share_activity/index.html"},
    {title:"（new）下载链接",url:pageRoot+"/com/page/invitation/down.html"},
    {title:"（new）下载链接-短信",url:pageRoot+"/com/page/invitation/down_msg.html"},
    {title:"（new）下载链接-微信",url:pageRoot+"/com/page/invitation/down_wechat.html"},
    {title:"（new）下载链接-渠道",url:pageRoot+"/com/page/invitation/down_activity.html"},
    {title:"（new）注册-流量宝",url:pageRoot+"/com/page/invitation/registered_1.html"},
    {title:"（new）注册-魔积分",url:pageRoot+"/com/page/invitation/registered_2.html"},
    {title:"（new）注册-小米",url:pageRoot+"/com/page/invitation/registered_3.html"},
    {title:"（new）注册-酷划锁屏",url:pageRoot+"/com/page/invitation/registered_4.html"},
    {title:"（new）注册-交通918",url:pageRoot+"/com/page/invitation/registered_5.html"},
    {title:"（new）注册-今日头条",url:pageRoot+"/com/page/invitation/registered_6.html"},
    {title:"（new）注册-商旅wifi",url:pageRoot+"/com/page/invitation/registered_7.html"},
    {title:"（new）注册-车点点",url:pageRoot+"/com/page/invitation/registered_8.html"},
    {title:"（new）被邀请页",url:pageRoot+"/com/page/invitation/invit.html?ydjrcode=e3oyfc84"},
    {title:"（new）被邀请页-分享领红包",url:pageRoot+"/com/page/invitation/invit_share.html?ydjrcode=e3oyfc84"},
    {title:"十一放假公告",url:pageRoot+"/com/page/notice/notice_nationalDay.html"},
    {title:"十一活动",url:pageRoot+"/com/page/ten_activity/index.html"},
    {title:"圣诞活动",url:pageRoot+"/com/page/christmas_activity/index.html"},
    {title:"2018全场加息活动",url:pageRoot+"/com/page/raise_activity/index.html"},
    {title:"小寒活动",url:pageRoot+"/com/page/cold_activity/index.html"},
    {title:"梦想计划",url:pageRoot+"/com/page/dream_activity/index.html"},
    {title:"满标活动",url:pageRoot+"/com/page/fullPro_activity/index.html"},
    {title:"我的奖励",url:"http://localhost:8088/app/page/my_reward/my_reward.html"},
    {title:"收支明细",url:"http://localhost:8088/app/page/inexport_detail/inexport_detail.html"}

    // {title:"邀友公告",url:pageRoot+"/com/page/company/notice.html"},
]
for(var i=0;i<data.length;i++){
    var num=i+1;
    var div=document.createElement("div");
    div.innerHTML=num+"."+data[i].title+":<br><a href='"+data[i].url+"'>"+data[i].url+"</a>";
    document.getElementById("main").appendChild(div);
}