//包装函数
module.exports= function (grunt) {
    var files = grunt.file.expand('app/page/**/js/*.app');
    var concat= {
        'app/lab/js/all.min.js':[
            'app/lab/js/zepto.js',
            'app/lab/js/stoneMoney.js',
            'app/lab/js/mobile-util.js',
            'app/lab/js/init.js',
            'app/lab/js/activity.js'
        ],
        'com/lab/js/all.min.js':[
            'app/lab/js/zepto.js',
            'app/lab/js/stoneMoney.js',
            'app/lab/js/mobile-util.js',
            'app/lab/js/init.js',
            'app/lab/js/activity.js'
        ]
    };
    var jsFiles = [];
    files.forEach(function (file) {
      var filenamelist = file.split('/');
      var num = filenamelist.length;
      var folder = filenamelist[num - 3];
      var name = filenamelist[num - 1].replace(/\.app$/,'');
       concat['app/page/'+folder+'/js/'+name+'.js'] = grunt.file.read(file).split(',');
       jsFiles.push({
            expand:true,
            cwd:'app/',
            src:'page/'+folder+'/js/'+name+'.js',
            dest:"com/",
            extDot:"last",
            ext:".js"
       });
    });
    //任务配置，所有插件的配置信息
    grunt.initConfig({
        //获取package.json的信息
        pkg:grunt.file.readJSON('package.json'),
        //创建服务器
        connect: {  
            server: {  //设定server  
              options: {  
                port: 8088,   //设定端口  
                hostname: '*',   //URL名，设置成*即表示localhost  
                keepalive: false  //如不设定，建立的服务器只在grunt任务执行时才有效，结束之后就停止  
              }  
            }  
        },
        //uglify插件的配置信息
        uglify:{
            publicJs: {
                options: {
                    beautify: false,
                    stripBanners:true,
               		 banner:'/*! <%=pkg.name%>-<%=pkg.vertion%>.js */\n'
                },
                files:[
                
                    {
                        expand:true,
                        cwd:'app/',
                        src:'lab/js/*.js',
                        dest:"com/",
                        extDot:"last",
                        ext:".js"
                    }
                    
                ]
            },
            
            release:{
                files:concat
            },
            htmlJs: {
                options: {
                    beautify: false
                },
                files:jsFiles
            }
        },
        //cssmin插件信息配置
        usemin: {
            html: ['com/page/**/**/*.html']
        },
        cssmin:{
            options:{
                report:'gzip'
            },
            build:{
                files:[
                    {
                        expand:true,
                        cwd:"app",
                        src:['page/**/*.css'],
                        dest:"com",
                        extDot:"last",
                        ext:".css"
                    },
                    {
                        expand:true,
                        cwd:"src/lab/css",
                        src:['*.css','**/*.css'],
                        dest:"build/lab/css",
                        extDot:"last",
                        ext:".css"
                    }
                ]
            }
        },
        //watch插件配置信息
        watch:{
            build:{
                files:[
                    'app/page/**/css/*.less',
                    'app/page/**/css/**/*.less'
                ],
                tasks:['less'],
                options:{
                    spawn:false
                }
            }
        },
        clean:{
            dist:['com','build']
        },
        concat: concat,
        //less插件配置
        less: {

             main: {
                expand: true,
                cwd:"app/",
                src: [
                    'page/**/css/*.less',
                    'page/**/css/**/*.less'
                ],
                dest: 'app',
                ext: '.css'
            },
           
            dev: {
                options: {
                    compress: true,
                    yuicompress:false
                }
            }
        },
        copy: {
            //new
             main: {
//              flatten: true,
                //    filter: 'isFile',
                expand: true,
                cwd:"app/",
                src: ['page/**/img/*','page/**/img/*/*'],
                dest: 'com/'
            },
            jsMod: {
                expand: true,
                cwd: "app/",
                src: "page/**/mod/*",
                dest: "com"
            },
            fontFile:{
                expand:true,
                cwd:"app/",
                src:"page/**/fonts/*",
                dest:"com"
            },
            htmlFile:{
                expand:true,
                cwd:"app/",
                src:"page/**/*.html",
                dest:"com/"
            },
            indexFile:{
                expand:true,
                cwd:"app/",
                src:"*.html",
                dest:"com/"
            },
            indexjsFile:{
                expand:true,
                cwd:"app/",
                src:"*.js",
                dest:"com/"
            },
            jsFile:{
                expand:true,
                cwd:"app/",
                src:"page/**/js/_*.js",
                dest:"com/"
            },
            labFile:{
                expand:true,
                cwd:"app/",
                src:"lab/css/*",
                dest:"com"
             }
            
            // build:{
            //     expand:true,
            //     cwd:'/Users/lixue/project/wap/ydjr_old/build',
            //     src:"**/*",
            //     dest:"build/"
            // }
            
        },
        rev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 8
            },
            assets: {
                files: [{
                    src: ['com/page/**/css/*.css','com/page/**/css/**/*.css','com/page/**/js/**/*.js','com/page/**/js/*.js','com/lab/css/*.css','com/lab/js/*.js']
                }]
            }
         }
        
    });
    //grunt加载clean插件
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks('grunt-cachebuster');
    grunt.loadNpmTasks('grunt-contrib-connect');
    //grunt加载uplify插件
    grunt.loadNpmTasks("grunt-contrib-uglify");

    //grunt加载cssmin插件
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    //grunt加载less插件
    grunt.loadNpmTasks("grunt-contrib-less");

    //grunt加载copy插件
    grunt.loadNpmTasks("grunt-contrib-copy");

    //grunt加载watch插件
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-usemin');


   
    //告诉grunt当我们在终端输入grunt时需要做些什么（注意先后顺序）
    grunt.registerTask("default",["clean","uglify","less","cssmin","copy","rev","usemin","connect","watch"]);
    grunt.registerTask('watcher', ['watch']);
};